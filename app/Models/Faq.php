<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Faq extends BaseModel
{
    use SoftDeletes, HasTranslations;

    protected $dates = ['deleted_at'];
    protected $fillable = ['question', 'answer'];

    public $translatable = ['question', 'answer'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(CategoryFaq::class,'category_faq_id','id');
    }

    /**
     * Custom method that searches through model translations.
     *
     * @param $field
     * @param $value
     * @return Builder
     */
    public static function whereTranslation($field, $value)
    {
        return static::where($field, 'like', '%' . $value . '%');
    }
}
