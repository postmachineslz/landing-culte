<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\Translatable\HasTranslations;

class CategoryFaq extends BaseModel
{
    use Sluggable, HasTranslations;

    protected $table = 'category_faq';
    protected $fillable = ['name', 'slug', 'status'];

    public $translatable = ['name', 'slug'];

    /**
     * Custom method that searches through model translations.
     *
     * @param $field
     * @param $value
     * @return Builder
     */
    public static function whereTranslation($field, $value)
    {
        return static::where($field, 'like', '%' . $value . '%');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * @param $value
     */
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = (boolean) $value;
    }

    /**
     * @param bool $formated
     * @return mixed
     * @internal param $status
     */
    public static function getStatus($formated = false)
    {
        $status = [
            1 => trans('painel.models.crud.status.active'),
            2 => trans('painel.models.crud.status.inactive'),
        ];
        if (!is_null($formated)) {
            $status = [
                1 => '<span class="label label-success"> '. trans('painel.models.crud.status.active') . '</span>',
                2 => '<span class="label label-danger"> '. trans('painel.models.crud.status.inactive') . '</span>',
            ];
            return $status[$formated];
        }
        return $status;
    }

    /**
     * @return mixed
     * @internal param $status
     */
    public static function statusList()
    {
        $status = [
            1 => trans('painel.models.crud.status.active'),
            2 => trans('painel.models.crud.status.inactive'),
        ];
        return $status;
    }
}
