<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Reason extends BaseModel
{
    use SoftDeletes, HasTranslations;

    protected $dates = ['deleted_at'];
    protected $fillable = ['title', 'description', 'image'];

    public $translatable = ['title', 'description'];

    /**
     * Custom method that searches through model translations.
     *
     * @param $field
     * @param $value
     * @return Builder
     */
    public static function whereTranslation($field, $value)
    {
        return static::where($field, 'like', '%' . $value . '%');
    }
}
