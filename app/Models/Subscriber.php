<?php

namespace App\Models;

class Subscriber extends BaseModel
{
    protected $table = 'subscribers';
    protected $fillable = ['name', 'email', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function downloads()
    {
        return $this->belongsToMany('App\Models\Download', 'subscriber_download','subscriber_id', 'download_id');
    }

    /**
     * @param bool $formated
     * @return mixed
     * @internal param $status
     */
    public static function getStatus($formated = false)
    {
        $status = [
            1 => 'Inscrito',
            2 => 'Inativo'
        ];
        if (!is_null($formated)) {
            $status = [
                1 => '<span class="label label-success">Inscrito</span>',
                2 => '<span class="label label-danger">Inativo</span>',
            ];
            return $status[$formated];
        }
        return $status;
    }

    /**
     * @return mixed
     * @internal param $status
     */
    public static function statusList()
    {
        $status = [
            1 => 'Inscrito',
            2 => 'Inativo'
        ];
        return $status;
    }
}
