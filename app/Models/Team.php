<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'function', 'avatar', 'about', 'facebook', 'instagram', 'linkedin', 'status'];
}
