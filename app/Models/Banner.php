<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends BaseModel
{
    use SoftDeletes, HasTranslations;

    protected $dates = ['deleted_at'];
    protected $fillable = ['title', 'description', 'image', 'status'];

    public $translatable = ['title', 'description'];

    /**
     * Custom method that searches through model translations.
     *
     * @param $field
     * @param $value
     * @return Builder
     */
    public static function whereTranslation($field, $value)
    {
        // Still not optimal solution ...
        return static::where($field, 'like', '%' . $value . '%');
    }

    /**
     * @param bool $formated
     * @return mixed
     * @internal param $status
     */
    public static function getStatus($formated = false)
    {
        $status = [
            1 => trans('painel.models.crud.status.active'),
            2 => trans('painel.models.crud.status.inactive'),
        ];
        if (!is_null($formated)) {
            $status = [
                1 => '<span class="label label-success"> '. trans('painel.models.crud.status.active') . '</span>',
                2 => '<span class="label label-danger"> '. trans('painel.models.crud.status.inactive') . '</span>',
            ];
            return $status[$formated];
        }
        return $status;
    }

    /**
     * @return mixed
     * @internal param $status
     */
    public static function statusList()
    {
        $status = [
            1 => trans('painel.models.crud.status.active'),
            2 => trans('painel.models.crud.status.inactive'),
        ];
        return $status;
    }
}
