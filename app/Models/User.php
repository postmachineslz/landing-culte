<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // /**
    //  * @param $pass
    //  */
    // public function setPasswordAttribute($pass){

    //     $this->attributes['password'] = bcrypt($pass);
    // }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'string|min:3|max:255',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|string|min:6|confirmed',
        ];
    }

    /**
     * @return array
     */
    public function rulesUpdateProfile()
    {
        $rules = $this->rules();

        unset($rules['password']);
        unset($rules['email']);

        return $rules;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function profileUpdate(array $data)
    {
        return $this->update($data);
    }

    /**
     * @param $newPassword
     * @return bool
     */
    public function updatePassword($newPassword)
    {
        $newPassword = bcrypt($newPassword);

        return $this->update([
            'password' => $newPassword,
        ]);
    }
}
