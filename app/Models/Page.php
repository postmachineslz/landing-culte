<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends BaseModel
{
    use SoftDeletes, Sluggable, HasTranslations;

    protected $dates = ['deleted_at'];
    protected $fillable = ['title', 'slug', 'image', 'desc', 'status'];

    public $translatable = ['title', 'slug', 'desc'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Custom method that searches through model translations.
     *
     * @param $field
     * @param $value
     * @return Builder
     */
    public static function whereTranslation($field, $value)
    {
        // Support for MariaDB and Mysql < 5.7
        // Still not optimal solution ...
        return static::where($field, 'like', '%' . $value . '%');
    }

//    /**
//     * @param $value
//     */
//    public function setSlugAttribute($value)
//    {
//        $this->attributes['slug'] = str_slug($value, '-');
//    }

    /**
     * @param $value
     */
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = (boolean) $value ?? true;
    }

    /**
     * @param bool $formated
     * @return mixed
     * @internal param $status
     */
    public static function getStatus($formated = false)
    {
        $status = [
            1 => trans('painel.models.crud.status.active'),
            2 => trans('painel.models.crud.status.inactive'),
        ];
        if (!is_null($formated)) {
            $status = [
                1 => '<span class="label label-success"> '. trans('painel.models.crud.status.active') . '</span>',
                2 => '<span class="label label-danger"> '. trans('painel.models.crud.status.inactive') . '</span>',
            ];
            return $status[$formated];
        }
        return $status;
    }

    /**
     * @return mixed
     * @internal param $status
     */
    public static function statusList()
    {
        $status = [
            1 => trans('painel.models.crud.status.active'),
            2 => trans('painel.models.crud.status.inactive'),
        ];
        return $status;
    }
}
