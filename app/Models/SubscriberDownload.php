<?php

namespace App\Models;

class SubscriberDownload extends BaseModel
{
    protected $table = 'subscriber_download';
    protected $fillable = ['subscriber_id', 'download_id', 'token', 'validate'];
}
