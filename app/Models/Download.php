<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Download extends BaseModel
{
    use SoftDeletes, HasTranslations;

    protected $dates = ['deleted_at'];
    protected $fillable = ['file', 'status', 'qtd_downloads'];
    public $translatable = ['file'];

    /**
     * Custom method that searches through model translations.
     *
     * @param $field
     * @param $value
     * @return Builder
     */
    public static function whereTranslation($field, $value)
    {
        return static::where($field, 'like', '%' . $value . '%');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subscribers()
    {
        return $this->belongsToMany('App\Models\Subscriber', 'subscriber_download', 'download_id', 'subscriber_id');
    }

    /**
     * @param bool $formated
     * @return mixed
     * @internal param $status
     */
    public static function getStatus($formated = false)
    {
        $status = [
            1 => trans('painel.models.crud.status.active'),
            2 => trans('painel.models.crud.status.inactive'),
        ];
        if (!is_null($formated)) {
            $status = [
                1 => '<span class="label label-success"> '. trans('painel.models.crud.status.active') . '</span>',
                2 => '<span class="label label-danger"> '. trans('painel.models.crud.status.inactive') . '</span>',
            ];
            return $status[$formated];
        }
        return $status;
    }

    /**
     * @return mixed
     * @internal param $status
     */
    public static function statusList()
    {
        $status = [
            1 => trans('painel.models.crud.status.active'),
            2 => trans('painel.models.crud.status.inactive'),
        ];
        return $status;
    }
}
