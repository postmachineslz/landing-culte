<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          =>  'required',
            'function'      =>  'required',
            'avatar'        =>  'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'about'         =>  'required',
            'facebook'      =>  'required',
            'instagram'     =>  'required',
            'linkedin'      =>  'required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'     =>  'O campo NOME não pode ficar vazio',

            'function.required' =>  'O campo FUNÇÃO não pode ficar vazio',

            'avatar.required'   =>  'O campo AVATAR não pode ficar vazio',
            'avatar.image'      =>  'Formato de imagem inválido',

            'about.required'    =>  'O campo SOBRE não pode ficar vazio',

            'facebook.required' =>  'O campo FACEBOOK não pode ficar vazio',

            'instagram.required' =>  'O campo INSTAGRAM não pode ficar vazio',

            'linkedin.required' =>  'O campo LINKEDIN não pode ficar vazio',
        ];
    }
}
