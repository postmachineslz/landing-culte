<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactMailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required|min:3',
            'phone'     =>  'required',
            'email'     =>  'required|email',
            'function'  =>  'min:3',
            'subject'   =>  'min:4',
            'message'   =>  'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => 'O campo nome precisa ser preenchido',
            'name.min'          => 'O campo nome deve conter pelo menos 3 caracteres',
            'phone.required'    => 'O campo telefone precisa ser preenchido',
            'email.email'       => 'Formato de email inválido',
            'function.min'      => 'O campo profissão precisa de pelo menos 3 caracteres',
            'subject.min'       => 'O campo assunto precisa de pelo menos 4 caracteres',
            'message.required'  => 'O campo telefone precisa ser preenchido',
        ];
    }
}
