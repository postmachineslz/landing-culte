<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryFaqStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required:unique:category_faq|max:100|min:3',
            'slug'  => 'required:unique:category_faq|max:100',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' =>  'O campo NOME precisa não pode ficar vazio',
            'name.unique'   =>  'Já existe uma categoria com esse mesmo nome',
            'name.min'      =>  'O campo NOME precisa de no mínimo 3 caracteres',
            'name.max'      =>  'O campo NOME pode conter no máximo 100 caracteres',
        ];
    }
}
