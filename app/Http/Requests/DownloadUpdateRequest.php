<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DownloadUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "file" => "required|mimes:pdf|max:1000"
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'file.required' =>  'Este campo não pode ficar vazio',
            'file.mimes'    =>  'Formato de arquivo não suportado. Por favor, envie somente arquivos PDF',
            'file.max'      =>  'Tamanho de arquivo não permitido',
        ];
    }
}
