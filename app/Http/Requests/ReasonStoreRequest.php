<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReasonStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         =>  'required',
            'description'   =>  'required',
            'image'         =>  'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'    =>  'O campo TÍTULO não pode ficar vazio',

            'description.required'    =>  'O campo DESCRIÇÃO não pode ficar vazio',

            'image.required'   =>  'O campo IMAGEM não pode ficar vazio',
            'image.image'      =>  'Formato de imagem inválido',
            'image.max'        =>  'Tamanho não suportado',
        ];
    }
}
