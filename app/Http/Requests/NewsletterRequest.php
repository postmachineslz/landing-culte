<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsletterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|min:3',
            'email' => 'required|email',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
          'name.required'   => 'O Campo Nome não pode ficar vazio',
          'email.required'  => 'O Campo Email precisa ser preenchido',
          'email.email'     => 'Formato de email inválido',
        ];
    }
}
