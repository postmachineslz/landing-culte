<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         =>  'required',
            'description'   =>  'required',
            'icon'          =>  'image|mimes:jpeg,png,jpg,svg|max:2048',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'            =>  'O campo TÍTULO não pode ficar vazio',
            'description.required'      =>  'O campo DESCRIÇÃO não pode ficar vazio',
            'icon.required'             =>  'O campo ÍCONE não pode ficar vazio',
            'icon.image'                =>  'Formato inválido',
            'icon.max'                  =>  'Tamanho não suportado',
        ];
    }
}
