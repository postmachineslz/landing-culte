<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         =>  'required|min:8|max:100',
            'description'   =>  'required|min:8|max:100',
            'image'         =>  'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'    =>  'O campo TÍTULO não pode ficar vazio',
            'title.min'         =>  'O campo TÍTULO precisa de no mínimo 8 caracteres',
            'title.max'         =>  'O campo TÍTULO pode conter no máximo 100 caracteres',

            'description.required'    =>  'O campo DESCRIÇÃO não pode ficar vazio',
            'description.min'         =>  'O campo DESCRIÇÃO precisa de no mínimo 8 caracteres',
            'description.max'         =>  'O campo DESCRIÇÃO pode conter no máximo 100 caracteres',

            'image.required'   =>  'O campo IMAGEM não pode ficar vazio',
            'image.image'      =>  'Formato de imagem inválido',
            'image.max'        =>  'Tamanho não suportado',
        ];
    }
}
