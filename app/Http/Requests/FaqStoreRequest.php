<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_faq_id'   =>  'required',
            'question'          =>  'required|min:8',
            'answer'            =>  'required|min:8',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'category_faq_id.required'      =>  'O campo CATEGORIA precisa ser preenchido',
            'question.required'             =>  'O campo PERGUNTA precisa ser preenchido',
            'answer.required'               =>  'O campo RESPOSTA precisa ser preenchido',
        ];
    }
}
