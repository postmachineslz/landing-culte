<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\CategoryFaq;
use App\Models\Download;
use App\Models\Page;
use App\Models\Reason;
use App\Models\Team;
use App\Models\Work;
use function Couchbase\defaultDecoder;
use JonathanTorres\MediumSdk\Medium;

class HomeController extends Controller
{
    protected $banner;
    protected $categoryFaq;
    protected $reason;
    protected $team;
    protected $work;
    protected $download;
    protected $page;

    /**
     * HomeController constructor.
     * @param Banner $banner
     * @param CategoryFaq $categoryFaq
     * @param Reason $reason
     * @param Team $team
     * @param Work $work
     * @param Download $download
     * @param Page $page
     */
    public function __construct(Banner $banner, CategoryFaq $categoryFaq, Reason $reason, Team $team, Work $work, Download $download, Page $page)
    {
        $this->banner       = $banner;
        $this->categoryFaq  = $categoryFaq;
        $this->reason       = $reason;
        $this->team         = $team;
        $this->work         = $work;
        $this->download     = $download;
        $this->page         = $page;
    }

    /**
     * @return $this
     */
    public function index()
    {
        $banners = $this->banner::where('status', 1)->take(1)->latest()->get();

        $categoriesFaq = $this->categoryFaq::where('status', 1)->get();
        $reasons = $this->reason::get();
        $teams = $this->team::get();
        $works = $this->work::get();
        $download = $this->download::where('status', 1)->first();

        $invista = $this->page::whereTranslation('id', 1)->first();
        $page = $this->page::whereTranslation('id', 2)->first();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://medium.com/culte/latest?format=json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        $data = str_replace("])}while(1);</x>", "", $result);
        $json = json_decode($data);
        $posts = $json->payload->posts;
        curl_close ($ch);

        return view('home', compact('banners', 'posts', 'categoriesFaq', 'reasons', 'teams', 'works', 'download', 'page', 'invista'))
            ->with('titlePage', trans('pages.meta.subtitle'));
    }

    /**
     * @param $url
     * @return mixed
     */
    public function getPostsMedium($url)
    {
        $headers = [
            'Content-Type: application/json',
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        $medium = json_decode($response);

        if ($medium) {
            return $medium->payload->posts;
        }
        return [];
    }
}
