<?php

namespace App\Http\Controllers;

use App\Models\Download;
use App\Models\Subscriber;
use App\Models\SubscriberDownload;
use App\Mail\SendLinkDownloadPDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DownloadsController extends Controller
{
    public $mailchimp;
    public $listId = 'c388305f9d';

    protected $download;
    protected $subscriber;
    protected $subscriberDownload;

    /**
     * FaqsController constructor.
     * @param \Mailchimp $mailchimp
     * @param Download $download
     * @param Subscriber $subscriber
     * @param SubscriberDownload $subscriberDownload
     */
    public function __construct(\Mailchimp $mailchimp, Download $download, Subscriber $subscriber, SubscriberDownload $subscriberDownload)
    {
        $this->mailchimp = $mailchimp;
        $this->download = $download;
        $this->subscriber = $subscriber;
        $this->subscriberDownload = $subscriberDownload;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Request $request)
    {
        DB::beginTransaction();

        $data = $request->all();

        $download = $this->download::withTrashed()->findOrFail(1);

        $subscriber = $this->subscriber->create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'status'    => 1,
        ]);

        $this->mailchimp
            ->lists
            ->subscribe(
                $this->listId,
                ['email' => $subscriber->email]
            );

        $subscriberDownload = $this->subscriberDownload->create([
            'subscriber_id' => $subscriber->id,
            'download_id'   => $download->id,
            'token'         => $this->generateToken(),
            'validate'      => Carbon::now()->addHour(),
        ]);

        if ($download && $subscriber && $subscriberDownload) {
            DB::commit();
            Mail::to($subscriber->email)->send(new SendLinkDownloadPDF($subscriber, $subscriberDownload->token));
            session()->flash('success', 'Você receberá um email contendo um link para baixar o nosso WhitePaper');

            return redirect()->route('site');
        } else {
            DB::rollBack();
            session()->flash('danger', 'Desculpe, ocorreu um erro. Por favorn, retorne mais tarde!');

            return redirect()->back();
        }
    }

    /**
     * @param $token
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadPDF($token)
    {
        $downloadId = $this->subscriberDownload::where('token', $token)->first();
        $download = $this->download::withTrashed()->findOrFail($downloadId->download_id);

        if ($downloadId && $download) {
            if ($downloadId->validate < Carbon::now()) {
                session()->flash('danger', 'O link expirou!');
                return redirect()->route('site');
            } else {
                $fileToDownload = public_path() . '/uploads/downloads/' . $download->file;
                $headers = ['Content-Type: application/pdf'];
                $newName = 'white-paper-' . time() . '.pdf';

                $download->increment('qtd_downloads');

                return response()->download($fileToDownload, $newName, $headers);
            }
        }
    }

    /**
     * @param int $length
     * @param bool $uppercase
     * @param bool $number
     * @param bool $symbol
     * @return string
     */
    public static function generateToken($length = 31, $uppercase = true, $number = true, $symbol = false)
    {
        $lmin           = 'abcdefghijklmnopqrstuvwxyz';
        $lmai           = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num            = '1234567890';
        $simb           = '!@#$%*-';
        $return         = '';
        $caracters      = '';
        $caracters      .= $lmin;

        if ($uppercase) $caracters .= $lmai;
        if ($number) $caracters .= $num;
        if ($symbol) $caracters .= $simb;

        $len = strlen($caracters);

        for ($n = 1; $n <= $length; $n++) {
            $rand = mt_rand(1, $len);
            $return .= $caracters[$rand-1];
        }
        return $return;
    }
}
