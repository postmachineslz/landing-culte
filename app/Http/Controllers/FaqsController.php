<?php

namespace App\Http\Controllers;

use App\Models\Download;
use App\Models\Faq;
use App\Models\CategoryFaq;

class FaqsController extends Controller
{
    protected $faq;
    protected $categoryFaq;
    protected $download;

    /**
     * FaqsController constructor.
     * @param Faq $faq
     * @param CategoryFaq $categoryFaq
     */
    public function __construct(Faq $faq, CategoryFaq $categoryFaq, Download $download)
    {
        $this->faq = $faq;
        $this->categoryFaq = $categoryFaq;
        $this->download = $download;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $faqs = $this->faq::paginate(20);
        $categoriesFaq = $this->categoryFaq::where('status', 1)->get();
        $download = $this->download::where('status', 1)->first();

        return view('faqs.index', compact('faqs', 'categoriesFaq', 'download'))->with('titlePage', 'Faq');
    }

    /**
     * @param $categorySlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($categorySlug)
    {
        $category = $this->categoryFaq->whereTranslation('slug', $categorySlug)->first();
        $categoriesFaq = $this->categoryFaq::whereTranslation('status', 1)->get();
        $faqs = $this->faq->whereTranslation('category_faq_id', $category->id)->get();
        $download = $this->download::where('status', 1)->first();

        return view("faqs.category", compact('faqs','categoriesFaq', 'category', 'download'))->with('titlePage', $category->name);
    }
}
