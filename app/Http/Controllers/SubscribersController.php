<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use App\Http\Requests\NewsletterRequest;
use App\Mail\UserSubscribeNewsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mailchimp_Error;
use Mailchimp_List_AlreadySubscribed;


class SubscribersController extends Controller
{
    public $mailchimp;
    public $listId = '450a80aace';

    /**
     * SubscribersController constructor.
     * @param $mailchimp
     */
    public function __construct(\Mailchimp $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

    /**
     * @param NewsletterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(NewsletterRequest $request)
    {
        if (request()->ajax()) {
            $subscribe = new Subscriber;
            $subscribe->email = $request->email;
            $subscribe->name = $request->name;
            $subscribe->status = 1;
            $subscribe->save();

            try {
                $this->mailchimp
                    ->lists
                    ->subscribe(
                        $this->listId,
                        ['email' => $request->email]
                    );
                Mail::to($subscribe)->send(new UserSubscribeNewsletter($subscribe));
            } catch (Mailchimp_List_AlreadySubscribed $e) {
                return response()->json('Este email já consta em nossa lista de email', 417);
            } catch (Mailchimp_Error $e) {
                return response()->json('Ocorreu um erro: ' . $e->getMessage(), 417);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendCompaign(Request $request)
    {
        $this->validate($request, [
            'subject'       => 'required',
            'to_email'      => 'required',
            'from_email'    => 'required',
            'message'       => 'required',
        ]);

        try {
            $options = [
                'list_id'       => $this->listId,
                'subject'       => $request->input('subject'),
                'from_name'     => $request->input('from_email'),
                'from_email'    => 'contato@cultecoin.com',
                'to_name'       => $request->input('to_email')
            ];

            $content = [
                'html' => $request->input('message'),
                'text' => strip_tags($request->input('message'))
            ];

            $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
            $this->mailchimp->campaigns->send($campaign['id']);

            return redirect()->back()->with('success','Campanha salva com sucesso');

        } catch (\Exception $e) {
            return redirect()->back()->with('error','Ocorreu um erro. Por favor, tente novamente');
        }
    }
}
