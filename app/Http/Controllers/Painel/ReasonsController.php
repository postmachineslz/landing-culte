<?php

namespace App\Http\Controllers\Painel;

use App\Models\Reason;
use Illuminate\Http\Request;
use App\Http\Requests\ReasonStoreRequest;
use App\Http\Requests\ReasonUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ReasonsController extends Controller
{
    protected $reason;

    /**
     * FaqsController constructor.
     * @param Reason $reason
     */
    public function __construct(Reason $reason)
    {
        $this->reason = $reason;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $reasons = $this->reason->paginate(20);

        return view('painel.reasons.index', compact('reasons'))->with('titlePage', trans('painel.models.crud.index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Reason $reason
     * @return \Illuminate\Http\Response
     */
    public function create(Reason $reason)
    {
        return view('painel.reasons.create', compact('reason'))->with('titlePage', trans('painel.models.crud.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReasonStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReasonStoreRequest $request)
    {
        $data = $request->all();
        $filename = '';
        if ($request->hasFile('image'))
        {
            $file       = $request->file('image', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/reasons/' . "reason-" . $filename);
        }
        $data['image'] = "reason-" . $filename;

        $create = $this->reason->create($data);

        if ($create) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('reasons.edit', $create->id);
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reason = $this->reason::withTrashed()->findOrFail($id);

        return view('painel.reasons.show', compact('reason'))->with('titlePage', $reason->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reason = $this->reason::withTrashed()->findOrFail($id);
        return view('painel.reasons.edit', compact('reason'))->with('titlePage', trans('painel.models.crud.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReasonUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReasonUpdateRequest $request, $id)
    {
        $reason = $this->reason::withTrashed()->findOrFail($id);
        $data = $request->all();

        if ($request->hasFile('image'))
        {
            if ($reason->image) {
                $imageUrl = public_path() . '/img/uploads/reasons/' . $reason->image;
                $reason->image = '';
                $reason->update();
                File::delete($imageUrl);
            }
            $file       = $request->file('image', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/reasons/' . "reason-" . $filename);
            $data['image'] = "reason-" . $filename;
        }

        $update = $reason->update($data);

        if ($update) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.edit'));
            return redirect()->route('reasons.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $delete = $this->reason::findOrFail($id)->delete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('reasons.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back();
    }

    /**
     * Restore
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $reason = $this->reason::withTrashed()->where('id', $id)->first();

        $restore = $reason->restore();

        if ($restore) {
            session()->flash('success', trans('painel.controllers.flash-messages.error.restore'));
            return redirect()->route('reasons..index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.restore'));
        return redirect()->back();

    }

    /**
     * @return $this
     */
    public function trashed()
    {
        $reasons = $this->reason::onlyTrashed()->paginate();

        return view('painel.reasons.trashed', compact('reasons'))->with('titlePage', trans('painel.models.crud.trasheds'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function forceDelete($id)
    {
        $reason = $this->reason::findOrFail($id);
        if ($reason->image){
            $imageUrl = public_path() . '/img/uploads/reasons/' . $reason->image;
            File::delete($imageUrl);
        }

        $delete = $reason->forceDelete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('reasons.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back();
    }
}
