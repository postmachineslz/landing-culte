<?php

namespace App\Http\Controllers\Painel;

use App\Models\Download;
use App\Models\Subscriber;
use App\Http\Controllers\Controller;

class SubscribersController extends Controller
{
    protected $subscriber;
    protected $download;

    public function __construct (Subscriber $subscriber, Download $download)
    {
        $this->subscriber = $subscriber;
        $this->download = $download;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subscribers()
    {
        $subscribers = $this->subscriber::orderBy('created_at')->paginate(20);

        $subscribeCount = $subscribers->count();

        return view('painel.newsletters.subscribers', compact('subscribers', 'subscribeCount'))->with('titlePage', 'inscritos via Newsletter');
    }
}
