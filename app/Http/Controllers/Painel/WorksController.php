<?php

namespace App\Http\Controllers\Painel;

use App\Models\Work;
use App\Http\Requests\WorkStoreRequest;
use App\Http\Requests\WorkUpdateRequest;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class WorksController extends Controller
{
    protected $work;

    /**
     * FaqsController constructor.
     * @param Work $work
     */
    public function __construct(Work $work)
    {
        $this->work = $work;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $works = $this->work->paginate(20);

        return view('painel.works.index', compact('works'))->with('titlePage', trans('painel.models.crud.index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Work $work
     * @return \Illuminate\Http\Response
     */
    public function create(Work $work)
    {
        return view('painel.works.create', compact('work'))->with('titlePage', trans('painel.models.crud.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param WorkStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkStoreRequest $request)
    {
        $data = $request->all();
        $filename = '';
        if ($request->hasFile('icon'))
        {
            $file       = $request->file('icon', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/works/' . "work-" . $filename);
        }
        $data['icon'] = "work-" . $filename;

        $create = $this->work->create($data);

        if ($create) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('works.edit', $create->id);
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $work = $this->work::withTrashed()->findOrFail($id);

        return view('painel.works.show', compact('work'))->with('titlePage', $work->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $work = $this->work::withTrashed()->findOrFail($id);
        return view('painel.works.edit', compact('work'))->with('titlePage', trans('painel.models.crud.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param WorkUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkUpdateRequest $request, $id)
    {
        $work = $this->work::withTrashed()->findOrFail($id);
        $data = $request->all();

        if ($request->hasFile('icon'))
        {
            if ($work->icon) {
                $iconUrl = public_path() . '/img/uploads/works/' . $work->icon;
                $work->icon = '';
                $work->update();
                File::delete($iconUrl);
            }
            $file       = $request->file('icon', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/works/' . "work-" . $filename);
            $data['icon'] = "work-" . $filename;
        }

        $update = $work->update($data);

        if ($update) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.edit'));
            return redirect()->route('works.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $delete = $this->work::findOrFail($id)->delete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('works.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }

    /**
     * Restore
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $work = $this->work::withTrashed()->where('id', $id)->first();

        $restore = $work->restore();

        if ($restore) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('works.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();

    }

    /**
     * @return $this
     */
    public function trashed()
    {
        $works = $this->work::onlyTrashed()->paginate();

        return view('painel.works.trashed', compact('works'))->with('titlePage', trans('painel.models.crud.trasheds'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function forceDelete($id)
    {
        $work = $this->work::findOrFail($id);
        if ($work->icon){
            $iconUrl = public_path() . '/img/uploads/works/' . $work->icon;
            File::delete($iconUrl);
        }

        $delete = $work->forceDelete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('works.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back();
    }
}
