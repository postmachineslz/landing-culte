<?php

namespace App\Http\Controllers\Painel;

use App\Models\Page;
use App\Http\Requests\PageStoreRequest;
use App\Http\Requests\PageUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class PagesController extends Controller
{
    protected $page;

    /**
     * PagesController constructor.
     * @param Page $page
     */
    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = $this->page::orderBy('id', 'ASC')->paginate(10);

        return view('painel.pages.index', compact('pages'))->with('titlePage', trans('painel.models.crud.index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = $this->page;
        return view('painel.pages.create', compact('page'))->with('titlePage', trans('painel.models.crud.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PageStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageStoreRequest $request)
    {
        $data = $request->all();
        //$data['slug'] = str_slug($data['title'], '-');

        $filename = '';
        if ($request->hasFile('image'))
        {
            $file       = $request->file('image', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/pages/' . "page-" . $filename);
        }
        $data['image'] = "page-" . $filename;

        $create = $this->page->create($data);

        if ($create) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('pages.edit', $create->id);
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = $this->page::withTrashed()->findOrFail($id);
        return view('painel.pages.show', compact('page'))->with('titlePage', $page->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = $this->page::statusList();
        $page = $this->page::withTrashed()->findOrFail($id);
        return view('painel.pages.edit', compact('page', 'status'))->with('titlePage', trans('painel.models.crud.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PageUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageUpdateRequest $request, $id)
    {
        $page = $this->page::withTrashed()->findOrFail($id);
        $data = $request->all();
        $data['slug'] = str_slug($data['title'], '-');

        if ($request->hasFile('image'))
        {
            if ($page->image) {
                $imageUrl = public_path() . '/img/uploads/pages/' . $page->image;
                $page->image = '';
                $page->update();
                File::delete($imageUrl);
            }
            $file       = $request->file('image', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/pages/' . "page-" . $filename);
            $data['image'] = "page-" . $filename;
        }

        $page = $this->page::withTrashed()->findOrFail($id)->update($data);

        if ($page) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.edit'));
            return redirect()->route('pages.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = $this->page::findOrFail($id);

        if ($page->image){
            $imageUrl = public_path() . '/img/uploads/pages/' . $page->image;
            File::delete($imageUrl);
        }

        $delete = $page->delete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('pages.index');
        }
        session()->flash('success', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back()->withInput();
    }

    /**
     * Restore
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $page = $this->page::withTrashed()->where('id', $id)->first();

        DB::beginTransaction();

        $restore = $page->restore();

        if ($restore && $page->deleted_at == null) {
            DB::commit();
            $page['status'] = 1;
            $page->update();
            session()->flash('success', trans('painel.controllers.flash-messages.success.restore'));
            return redirect()->route('pages.index');
        }
        DB::rollback();
        session()->flash('success', trans('painel.controllers.flash-messages.error.restore'));
        return redirect()->back();
    }

    /**
     * @return $this
     */
    public function trashed()
    {
        $pages = $this->page::onlyTrashed()->paginate();

        return view('painel.pages.trashed', compact('pages'))->with('titlePage', trans('painel.models.crud.trasheds'));
    }
}
