<?php

namespace App\Http\Controllers\Painel;

use App\Models\Faq;
use App\Models\CategoryFaq;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryFaqStoreRequest;
use App\Http\Requests\CategoryFaqUpdateRequest;
use Illuminate\Http\Request;

class CategoryFaqController extends Controller
{
    protected $faq;
    protected $categoryFaq;

    /**
     * CategoryFaqController constructor.
     * @param $faq
     * @param $categoryFaq
     */
    public function __construct(Faq $faq, CategoryFaq $categoryFaq)
    {
        $this->faq = $faq;
        $this->categoryFaq = $categoryFaq;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryFaq->paginate(15);

        return view('painel.category-faq.index', compact('categories'))->with('titlePage', trans('painel.models.crud.index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CategoryFaq $category
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryFaq $category)
    {
        return view('painel.category-faq.create', compact('category'))->with('titlePage', trans('painel.models.crud.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryFaqStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryFaqStoreRequest $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['name'], '-');

        $create = $this->categoryFaq->create($data);

        if ($create) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('category-faq.edit', $create->id);
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->categoryFaq::findOrFail($id);

        return view('painel.category-faq.show', compact('category'))->with('titlePage', $category->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryFaq::findOrFail($id);
        return view('painel.category-faq.edit', compact('category'))->with('titlePage', trans('painel.models.crud.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryFaqUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryFaqUpdateRequest $request, $id)
    {
        $category = $this->categoryFaq::findOrFail($id);
        $data = $request->all();
        $update = $category->update($data);

        if ($update) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.edit'));
            return redirect()->route('category-faq.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->categoryFaq::findOrFail($id)->delete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('category-faq.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }
}
