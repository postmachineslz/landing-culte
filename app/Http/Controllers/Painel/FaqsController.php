<?php

namespace App\Http\Controllers\Painel;

use App\Models\Faq;
use App\Models\CategoryFaq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqStoreRequest;
use App\Http\Requests\FaqUpdateRequest;

/**
 * @property CategoryFaq category
 */
class FaqsController extends Controller
{
    protected $faq;
    protected $category;

    /**
     * FaqsController constructor.
     * @param Faq $faq
     * @param CategoryFaq $category
     */
    public function __construct(Faq $faq, CategoryFaq $category)
    {
        $this->faq = $faq;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $faqs = $this->faq->with('category')->paginate(20);

        return view('painel.faqs.index', compact('faqs'))->with('titlePage', trans('painel.models.crud.index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function create(Faq $faq)
    {
        $categories = $this->category::get()->pluck('name', 'id');

        return view('painel.faqs.create', compact('categories', 'faq'))->with('titlePage', trans('painel.models.crud.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FaqStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqStoreRequest $request)
    {
        $data = $request->all();
        $create = $this->faq->create($data);

        if ($create) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('faq.edit', $create->id);
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $faq = $this->faq::withTrashed()->findOrFail($id);
        return view('painel.faqs.show', compact('faq'))->with('titlePage', $faq->question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = $this->category::get()->pluck('name', 'id');
        $faq = $this->faq::withTrashed()->findOrFail($id);

        return view('painel.faqs.edit', compact('faq', 'categories'))->with('titlePage', trans('painel.models.crud.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FaqUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqUpdateRequest $request, $id)
    {
        $data = $request->all();

        $update = $this->faq::withTrashed()->findOrFail($id)->update($data);

        if ($update) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.edit'));
            return redirect()->route('faq.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $delete = $this->faq::findOrFail($id)->delete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('faq.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }

    /**
     * Restore
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $faq = $this->faq::withTrashed()->where('id', $id)->first();

        $restore = $faq->restore();

        if ($restore) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('faq.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();

    }

    /**
     * @return $this
     */
    public function trashed()
    {
        $faqs = $this->faq::onlyTrashed()->paginate();

        return view('painel.faqs.trashed', compact('faqs'))->with('titlePage', trans('painel.models.crud.trasheds'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function forceDelete($id)
    {
        $delete = $this->faq::findOrFail($id)->forceDelete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->back();
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back();
    }
}
