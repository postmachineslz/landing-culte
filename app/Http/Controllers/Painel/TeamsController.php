<?php

namespace App\Http\Controllers\Painel;

use App\Models\Team;
use App\Http\Controllers\Controller;
use App\Http\Requests\TeamUpdateRequest;
use App\Http\Requests\TeamStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class TeamsController extends Controller
{
    protected $team;

    /**
     * FaqsController constructor.
     * @param Team $team
     */
    public function __construct(Team $team)
    {
        $this->team = $team;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $teams = $this->team->paginate(20);

        return view('painel.teams.index', compact('teams'))->with('titlePage', trans('painel.models.crud.index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Team $team
     * @return \Illuminate\Http\Response
     */
    public function create(Team $team)
    {
        return view('painel.teams.create', compact('team'))->with('titlePage', trans('painel.models.crud.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TeamStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamStoreRequest $request)
    {
        $data = $request->all();
        $filename = '';
        if ($request->hasFile('avatar'))
        {
            $file       = $request->file('avatar', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/teams/' . "team-" . $filename);
        }
        $data['avatar'] = "team-" . $filename;

        $create = $this->team->create($data);

        if ($create) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('teams.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = $this->team::withTrashed()->findOrFail($id);

        return view('painel.teams.show', compact('team'))->with('titlePage', $team->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = $this->team::withTrashed()->findOrFail($id);
        return view('painel.teams.edit', compact('team'))->with('titlePage', trans('painel.models.crud.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TeamUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamUpdateRequest $request, $id)
    {
        $team = $this->team::withTrashed()->findOrFail($id);
        $data = $request->all();

        if ($request->hasFile('avatar'))
        {
            if ($team->avatar) {
                $avatarUrl = public_path() . '/img/uploads/teams/' . $team->avatar;
                $team->avatar = '';
                $team->update();
                File::delete($avatarUrl);
            }
            $file       = $request->file('avatar', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/teams/' . "team-" . $filename);
            $data['avatar'] = "team-" . $filename;
        }

        $update = $team->update($data);

        if ($update) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.edit'));
            return redirect()->route('teams.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $delete = $this->team::findOrFail($id)->delete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('teams.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back();
    }

    /**
     * Restore
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $team = $this->team::withTrashed()->where('id', $id)->first();

        $restore = $team->restore();

        if ($restore) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.restore'));
            return redirect()->route('teams.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.restore'));
        return redirect()->back();

    }

    /**
     * @return $this
     */
    public function trashed()
    {
        $teams = $this->team::onlyTrashed()->paginate();

        return view('painel.teams.trashed', compact('teams'))->with('titlePage', trans('painel.models.crud.trasheds'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function forceDelete($id)
    {
        $team = $this->team::findOrFail($id);
        if ($team->avatar){
            $avatarUrl = public_path() . '/img/uploads/teams/' . $team->avatar;
            File::delete($avatarUrl);
        }

        $delete = $team->forceDelete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('teams.trashed');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back();
    }
}
