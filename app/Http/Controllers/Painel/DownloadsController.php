<?php

namespace App\Http\Controllers\Painel;

use App\Models\Download;
use App\Http\Requests\DownloadStoreRequest;
use App\Http\Requests\DownloadUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class DownloadsController extends Controller
{
    protected $download;

    /**
     * FaqsController constructor.
     * @param Download $download
     */
    public function __construct(Download $download)
    {
        $this->download = $download;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $download = $this->download::withTrashed()->findOrFail(1);

        return view('painel.downloads.index', compact('download'))->with('titlePage', trans('painel.models.crud.index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Download $download
     * @return \Illuminate\Http\Response
     */
    public function create(Download $download)
    {
        return view('painel.downloads.create', compact('download'))->with('titlePage', trans('painel.models.crud.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DownloadStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DownloadStoreRequest $request)
    {
        $data = $request->all();
        $filename = '';

        if ($request->hasFile('file')) 
        {
            $file           = $request->file('file', null);
            $filename       = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $upload         = move_uploaded_file($data['file'], public_path() . '/uploads/downloads/' . "download-" . $filename);
            $data['file']   = "download-" . $filename;
        }

        $create = $this->download->create($data);

        if ($create && $upload) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('downloads.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $download = $this->download::withTrashed()->findOrFail($id);

        return view('painel.downloads.show', compact('download'))->with('titlePage', trans('painel.models.crud.show'));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadFile($id)
    {
        $download = $this->download::withTrashed()->findOrFail($id);

        $fileToDownload = public_path() . '/uploads/downloads/' . $download->file;
        $headers = ['Content-Type: application/pdf'];
        $newName = 'white-paper-' . time() . '.pdf';

        if ($fileToDownload) {
            $download->increment('qtd_downloads');
        }

        return response()->download($fileToDownload, $newName, $headers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $download = $this->download::withTrashed()->findOrFail($id);
        return view('painel.downloads.edit', compact('download'))->with('titlePage', trans('painel.models.crud.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DownloadUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DownloadUpdateRequest $request, $id)
    {
        $download = $this->download::withTrashed()->findOrFail($id);
        $data = $request->all();

        if ($request->hasFile('file'))
        {
            if ($download->file) {
                $fileUrl = public_path() . '/uploads/downloads/' . $download->file;
                $download->file = '';
                File::delete($fileUrl);
            }

            $file           = $request->file('file', null);
            $filename       = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $upload         = move_uploaded_file($data['file'], public_path() . '/uploads/downloads/' . "download-" . $filename);
            $data['file']   = "download-" . $filename;

            $update = $download->update($data);
        }


        if ($update && $upload) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.edit'));
            return redirect()->route('downloads.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $delete = $this->download::findOrFail($id)->delete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->route('downloads.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back();
    }

    /**
     * Restore
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $download = $this->download::withTrashed()->where('id', $id)->first();

        $restore = $download->restore();

        if ($restore) {
            session()->flash('success', trans('painel.controllers.flash-messages.error.restore'));
            return redirect()->route('downloads.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.restore'));
        return redirect()->back();

    }

    /**
     * @return $this
     */
    public function trashed()
    {
        $downloads = $this->download::onlyTrashed()->paginate();

        return view('painel.downloads.trashed', compact('downloads'))->with('titlePage', trans('painel.models.crud.trasheds'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function forceDelete($id)
    {
        $download = $this->download::findOrFail($id);
        if ($download->file){
            $fileUrl = public_path() . '/uploads/downloads/' . $download->file;
            File::delete($fileUrl);
        }

        $delete = $download->forceDelete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.destroy'));
            return redirect()->back();
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.destroy'));
        return redirect()->back();
    }
}
