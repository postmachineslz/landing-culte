<?php

namespace App\Http\Controllers\Painel;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    protected $user;

    /**
     * FaqsController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $users = $this->user->paginate(20);

        return view('painel.users.index', compact('users'))->with('titlePage', 'Usuários');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        return view('painel.users.create', compact('user'))->with('titlePage', 'Adicionar Usuário');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $filename = '';
        if ($request->hasFile('avatar'))
        {
            $file       = $request->file('avatar', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/users/' . "user-" . $filename);
        }
        $data['avatar'] = "user-" . $filename;
        $data['password'] = bcrypt($data['password']);

        $create = $this->user->create($data);

        if ($create) {
            session()->flash('success', 'Adicionado com sucesso!');
            return redirect()->route('users.index');
        }
        session()->flash('danger', 'Desculpe, ocorreu um erro. Por favor, retorne mais tarde!');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->user::withTrashed()->findOrFail($id);

        return view('painel.users.show', compact('user'))->with('titlePage', $user->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user::withTrashed()->findOrFail($id);
        return view('painel.users.edit', compact('user'))->with('titlePage', 'Editar Usuário');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user::withTrashed()->findOrFail($id);
        $data = $request->all();

        $update = $user->update($data);

        if ($update) {
            session()->flash('success', 'Editado com sucesso!');
            return redirect()->route('users.index');
        }
        session()->flash('danger', 'Desculpe, ocorreu um erro. Por favor, retorne mais tarde!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $delete = $this->user::findOrFail($id)->delete();

        if ($delete) {
            session()->flash('success', 'Desativado com sucesso!');
            return redirect()->route('users.index');
        }
        session()->flash('danger', 'Desculpe, ocorreu um erro. Por favor, retorne mais tarde!');
        return redirect()->back();
    }

    /**
     * Restore
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $user = $this->user::withTrashed()->where('id', $id)->first();

        $restore = $user->restore();

        if ($restore) {
            session()->flash('success', 'Restaurado com sucesso!');
            return redirect()->route('users.index');
        }
        session()->flash('danger', 'Desculpe, ocorreu um erro. Por favor, retorne mais tarde!');
        return redirect()->back();

    }

    /**
     * @return $this
     */
    public function trashed()
    {
        $users = $this->user::onlyTrashed()->paginate();

        return view('painel.users.trashed', compact('users'))->with('titlePage', 'Usuários Desativados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function forceDelete($id)
    {
        $user = $this->user::findOrFail($id);

        $delete = $user->forceDelete();

        if ($delete) {
            session()->flash('success', 'Usuário removido permanentimente!');
            return redirect()->back();
        }
        session()->flash('danger', 'Desculpe, ocorreu um erro. Por favor, retorne mais tarde!');
        return redirect()->back();
    }
}
