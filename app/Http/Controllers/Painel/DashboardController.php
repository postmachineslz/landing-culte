<?php

namespace App\Http\Controllers\Painel;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\Analytics\Period;
use Spatie\Analytics\Analytics;
use App\Models\Subscriber;
use App\Models\SubscriberDownload;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public $subscriber;
    public $download;

    /**
     * DashboardController constructor.
     * @param Subscriber $subscriber
     * @param SubscriberDownload $download
     */
    public function __construct(Subscriber $subscriber, SubscriberDownload $download)
    {
        $this->subscriber = $subscriber;
        $this->download = $download;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $series = ['20,10'];
        $subscribers = $this->subscriber::where('status', 1)->get();
        $newSubscribers = $this->subscriber->select(DB::raw('count(id) as `total_subscribers`'), DB::raw("MONTH(created_at) as week"))
            ->groupby('week')
            ->where('status', 1)
            ->get();

        $canceledSubscribers = $this->subscriber::where('status', 2)->get();

        $query = DB::raw(
            "select count(download_id) as total_download, MONTH(created_at) as mes from subscriber_download group by mes"
        );
        $downloadSelect = DB::select($query);
        $downloads = json_decode(json_encode($downloadSelect),true);

        $totalDownloads = $this->download::all();

        $accessors = \Analytics::fetchTotalVisitorsAndPageViews(Period::days(7));

        $countAccessors = $accessors->sum('visitors');
        $countPageViews = $accessors->sum('pageViews');

        return view('painel.dashboard', compact('series', 'subscribers','newSubscribers', 'canceledSubscribers', 'downloads', 'totalDownloads', 'accessors', 'countAccessors', 'countPageViews'))->with('titlePage', 'Dashboard');
    }
}
