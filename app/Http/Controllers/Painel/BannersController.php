<?php

namespace App\Http\Controllers\Painel;

use App\Models\Banner;
use App\Http\Requests\BannerStoreRequest;
use App\Http\Requests\BannerUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class BannersController extends Controller
{
    protected $banner;

    /**
     * BannersController constructor.
     * @param Banner $banner
     */
    public function __construct(Banner $banner)
    {
        $this->banner = $banner;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = $this->banner::orderBy('title', 'ASC')->paginate(10);

        return view('painel.banners.index', compact('banners'))->with('titlePage', 'Banners');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banner = $this->banner;
        return view('painel.banners.create', compact('banner'))->with('titlePage', trans('painel.models.crud.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BannerStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerStoreRequest $request)
    {
        $data = $request->all();
        $filename = '';
        if ($request->hasFile('image'))
        {
            $file       = $request->file('image', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/banners/' . "banner-" . $filename);
        }
        $data['image'] = "banner-" . $filename;

        $create = $this->banner::create($data);

        if ($create) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('banners.edit', $create->id);
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = $this->banner::withTrashed()->findOrFail($id);
        return view('painel.banners.show', compact('banner'))
            ->with('titlePage', $banner->title);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = $this->banner::statusList();
        $banner = $this->banner::withTrashed()->findOrFail($id);
        return view('painel.banners.edit', compact('banner', 'status'))
            ->with('titlePage', trans('painel.models.crud.edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BannerUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerUpdateRequest $request, $id)
    {
        $banner = $this->banner::withTrashed()->findOrFail($id);
        $data = $request->all();
        $data['user_id'] = auth()->user()->getAuthIdentifier();

        if ($request->hasFile('image'))
        {
            if ($banner->image) {
                $imageUrl = public_path() . '/img/uploads/banners/' . $banner->image;
                $banner->image = '';
                $banner->update();
                File::delete($imageUrl);
            }
            $file       = $request->file('image', null);
            $filename   = sha1($file->getClientOriginalName() . microtime()) . '.' . $file->getClientOriginalExtension();
            $img        = Image::make($file);

            $img->save(public_path() . '/img/uploads/banners/' . "banner-" . $filename);
            $data['image'] = "banner-" . $filename;
        }

        $banner = $this->banner::withTrashed()->findOrFail($id)->update($data);

        if ($banner) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.edit'));
            return redirect()->route('banners.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = $this->banner::findOrFail($id);

        $delete = $banner->delete();

        if ($delete) {
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('banners.index');
        }
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }

    /**
     * Restore
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        $banner = $this->banner::withTrashed()->where('id', $id)->first();

        DB::beginTransaction();

        $restore = $banner->restore();

        if ($restore && $banner->deleted_at == null) {
            DB::commit();
            $banner['status'] = 1;
            $banner->update();
            session()->flash('success', trans('painel.controllers.flash-messages.success.create'));
            return redirect()->route('banners.index');
        }
        DB::rollback();
        session()->flash('danger', trans('painel.controllers.flash-messages.error.create'));
        return redirect()->back();
    }

    /**
     * @return $this
     */
    public function trashed()
    {
        $banners = $this->banner::onlyTrashed()->paginate();

        return view('painel.banners.trashed',
            compact('banners'))->with('titlePage', trans('painel.models.crud.trasheds'));
    }
}
