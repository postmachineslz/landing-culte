<?php

namespace App\Http\Controllers\Painel;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    protected $user;

    /**
     * ProfileController constructor.
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return $this
     */
    public function profile()
    {
        $user = auth()->user();
        return view('painel.profile.profile', compact('user'))->with('titlePage', 'Meu Perfil');
    }

    /**
     * @param Request $request
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileUpdate(Request $request, User $user)
    {
        $this->validate($request, $user->rulesUpdateProfile());

        $dataForm = $request->all();

        if(isset($dataForm['email'])) {
            unset($dataForm['email']);
        }

        $update = auth()->user()->profileUpdate($dataForm);

        if($update) {
            return redirect()->route('painel.profile')->with('message', 'Perfil Atualizado Sucesso!');
        }

        return redirect()->route('painel.profile')->with('error', 'Falha ao atualizar!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function password()
    {
        $user = auth()->user();
        return view('painel.profile.change-password', compact('user'))->with('titlePage', 'Minha Senha');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function passwordUpdate(Request $request)
    {
        $this->validate($request, ['password' => 'required|string|min:6|confirmed']);
        $update = auth()->user()->updatePassword($request->password);

        if($update){
            return redirect()->route('painel.password')->with('message', 'Senha Atualizada Sucesso!');
        }

        return redirect()->route('painel.password')->with('error', 'Falha ao atualizar!');
    }
}
