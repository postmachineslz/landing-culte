<?php

namespace App\Http\Controllers;

use App\Mail\ContactEmail;
use App\Http\Requests\ContactMailRequest;
use Illuminate\Support\Facades\Mail;

class ContactMailController extends Controller
{
    /**
     * @param ContactMailRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendMail(ContactMailRequest $request)
    {
        $contact = [];

        $contact['name']      = $request->get('name');
        $contact['phone']     = $request->get('phone');
        $contact['email']     = $request->get('email');
        $contact['function']  = $request->get('function');
        $contact['subject']   = $request->get('subject');
        $contact['message']   = $request->get('message');


        Mail::to(config('mail.from.address'))->send(new ContactEmail($contact));
        return response()->json(['message' => 'Email enviado com sucesso. Retornaremos seu contato em breve'],200);
    }
}
