<?php

namespace App\Mail;

use App\Models\Subscriber;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendLinkDownloadPDF extends Mailable
{
    use Queueable, SerializesModels;

    public $subscriber;
    public $token;

    /**
     * Create a new message instance.
     *
     * @param Subscriber $subscriber
     * @param $token
     */
    public function __construct(Subscriber $subscriber, $token)
    {
        $this->subscriber = $subscriber;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send-link-to-download-pdf')
            ->subject('Solicitação de Download de WhitePaper')
            ->with([
                'subscriber'    => $this->subscriber->name,
                'token'         => $this->token,
            ]);
    }
}
