<?php

namespace App\Mail;

use App\Models\Subscriber;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserSubscribeNewsletter extends Mailable
{
    use Queueable, SerializesModels;

    protected $newsletter;

    /**
     * Create a new message instance.
     *
     * @param Subscriber $newsletter
     */
    public function __construct(Subscriber $newsletter)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user-subscribe')
            ->subject('Você acaba de se inscrever na Subscriber do ' . config('app.name'))
            ->with(
                [
                    'name' => $this->newsletter->name,
                    'email' => $this->newsletter->email
                ]
            );
    }
}
