<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryFaqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_faq')->insert([
            [
                'id' => 1,
                'name' => 'Aquisição',
                'slug' => str_slug('Aquisiçao', '-'),
            ],
            [
                'id' => 2,
                'name' => 'Retorno',
                'slug' => str_slug('Retorno', '-'),
            ],
            [
                'id' => 3,
                'name' => 'Regulamentação',
                'slug' => str_slug('Regulamentação', '-'),
            ],
            [
                'id' => 4,
                'name' => 'Registro',
                'slug' => str_slug('Registro', '-'),
            ]
        ]);
    }
}
