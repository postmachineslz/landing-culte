<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'       => 'Flaviano Honorato',
                'email'      => 'flaviano.honorato@gmail.com',
                'password'   => 'secret'
            ]
        ];

        foreach($data as $user) {
            App\Models\User::create($user);
        }

        factory(App\Models\User::class, 10)->create();
    }
}
