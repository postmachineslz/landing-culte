<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(CategoryFaqTableSeeder::class);
         $this->call(FaqsTableSeeder::class);
         $this->call(ReasonsTableSeeder::class);
         $this->call(TeamTableSeeder::class);
         $this->call(WorkTableSeeder::class);
    }
}
