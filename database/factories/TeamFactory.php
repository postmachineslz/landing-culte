<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Team::class, function (Faker $faker) {
    return [
        'name'          =>  $faker->name,
        'function'      =>  $faker->jobTitle,
        'avatar'        =>  $faker->imageUrl($width = 100, $height = 100, 'people', true, 'Faker'),
        'about'         =>  $faker->sentence(20),
        'facebook'      =>  $faker->url,
        'instagram'     =>  $faker->url,
        'linkedin'      =>  $faker->url
    ];
});
