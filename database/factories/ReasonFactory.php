<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Reason::class, function (Faker $faker) {
    return [
        'title'         => $faker->sentence(5),
        'description'   => $faker->sentence(15),
        'image'         =>  $faker->imageUrl($width = 690, $height = 450, 'cats', true, 'Faker')
    ];
});
