<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Faq::class, function (Faker $faker) {
    return [
        'category_faq_id'   => \App\Models\CategoryFaq::all()->unique()->random()->id,
        'question'          => $faker->sentence(6),
        'answer'            => $faker->sentence(20)
    ];
});
