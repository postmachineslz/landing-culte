<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Work::class, function (Faker $faker) {
    $icons = [
        'item-work-1.png',
        'item-work-2.png',
        'item-work-3.png',
        'item-work-4.png',
    ];
    return [
        'title'        =>  $faker->sentence(2),
        'description'  =>  $faker->sentence(15),
        'icon'         =>  $faker->randomElement($icons)
    ];
});
