<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Instalador Laravel',
    'next' => 'Próximo Passo',
    'finish' => 'Instalar',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Bem-vindo ao Instalador',
        'message' => 'Bem-vindo ao assistente de configuração.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requisitos',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissões',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Configurações de Ambiente',
        'save' => 'Salvar .env',
        'success' => 'Suas configurações de arquivo .env foram gravadas.',
        'errors' => 'Não foi possível gravar o arquivo .env, por favor crie-o manualmente.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Terminado',
        'finished' => 'Aplicação foi instalada com sucesso',
        'exit' => 'Clique aqui para sair',
    ],

    'download' => [
        'success' => 'Thank you for your interest in Culte Coin, in an instant we will send you an email with our explanatory presentation.',
        'wait'  =>  'Wait...',
        'fail'  =>  'An error has occurred. Please try again.',
        'title_modal'   =>  'All right',
        'close_modal'   =>  'Close',
    ],
    'subscriber' => [
        'success' => 'Thank you for signing in, we will be back soon.',
        'fail' => 'An error has occurred. Please try again.',
        'error' =>  'Please fill in all the fields.'
    ],

    'contact' => [
        'success' => 'Thanks for getting back to us, we will get back to you as soon as possible.',
        'fail' => 'An error has occurred. Please try again.',
        'error' =>  'Please fill in all the fields.'
    ],
];
