<?php

return [
    'menu' => [
        'banners'       =>  'Banner',
        'pages'         =>  'Pages',
        'how_works'     =>  'How it Works',
        'reasons'       =>  'Reasons to Invest',
        'teams'         =>  'Team',
        'downloads'     =>  'Presentation',
        'faqs'          =>  'Faq',
        'faq-category'  =>  'Faq Category',
        'newsletters'   =>  'Newsletter',
        'users'         =>  'Users',
        'list'          =>  'View All',
        'create'        =>  'Create',
        'back'          =>  'Back',
        'cancel'        =>  'Cancel',
    ],
    'titles'    =>  [
        'dashboard'     =>  'Painel',
        'my-profile'    =>  'My Profile',
        'go-to-site'    =>  'Go to website',
        'logout'        =>  'Logout',
        'change-lang'   =>  'Change to'
    ],
    'dashboard' =>  [
        'subscribed-statistics' =>  [
            'title'     =>  'Subscribed Statistics',
            'subtitle'  =>  'Last Campaign Performance',
            'des1'      =>  'Subscribers',
            'des2'      =>  'Canceled',
        ],
        'new-subscribers'       =>  [
            'title'     =>  'New Subscribers',
            'subtitle'  =>  'Subscribers in recent months',
            'desc1'     =>  'Subscribers',
            'desc2'     =>  'Canceled',
        ],
        'downloads'             =>  [
            'title'     =>  'Downloads',
            'subtitle'  =>  'Downloads Statistics in the last month',
            'total'     =>  'Total Downloads'
        ],
        'access'  =>  [
            'title'     =>  'Access',
            'subtitle'  =>  'Access statistics for last 7 days',
            'total'     =>  'Total of the last 7 days',
            'desc1'     =>  'Visits',
            'desc2'     =>  'Page Views',
        ],
    ],
    'controllers'   =>  [
        'flash-messages'    =>  [
            'type'  =>  [
                'success'   =>  'Success',
                'info'      =>  'Info',
                'warning'   =>  'Warning',
                'danger'    =>  'Danger',
            ],
            'success' =>    [
                'create'    =>  'added successfully',
                'edit'      =>  'updated successfully',
                'destroy'   =>  'deactivated successfully',
                'restore'   =>  'restored successfully',
            ],
            'error' =>    [
                'create'    =>  'There was an error adding this record. Please try again',
                'edit'      =>  'Ocorreu um erro ao alterar este registro. Por favor, tente novamente',
                'destroy'   =>  'There was an error changing this record. Please try again',
                'restore'   =>  'There was an error restoring this record. Please try again',
            ],
        ]
    ],
    'models'    =>  [
        'crud'    =>  [
            'actions'       =>  'Actions',
            'index'         =>  'List of Records',
            'show'          =>  'Details',
            'create'        =>  'Create',
            'edit'          =>  'Edit',
            'delete'        =>  'Remove',
            'trasheds'      =>  'Trasheds',
            'restore'       =>  'Restore',
            'crated_at'     =>  'Created at',
            'update_at'     =>  'Updated at',
            'deleted_at'    =>  'Deleted at',
            'status'    =>  [
                'active'    =>  'Active',
                'inactive'    =>  'Inactive',
            ]
        ],
        'banners'   =>  [
            'title'     =>  'Title',
            'desc'      =>  'Description',
            'image'     =>  'Image',
        ],
        'category_faq'  =>  [
            'name'     =>  'Category',
        ],
        'faqs'  =>  [
            'question'     =>  'Question',
            'answer'       =>  'Answer',
        ],
        'how_works'  =>  [
            'title'         =>  'Títle',
            'image'         =>  'Image',
            'description'   =>  'Description',
        ],
        'pages'  =>  [
            'title'         =>  'Títle',
            'image'         =>  'Image',
            'description'   =>  'Description',
        ],
        'reasons'  =>  [
            'title'         =>  'Títle',
            'description'   =>  'Description',
            'image'         =>  'Image',
        ],
        'teams'  =>  [
            'name'          =>  'name',
            'function'      =>  'Function',
            'avatar'        =>  'Avatar',
            'about'         =>  'About',
        ],
        'users'  =>  [
            'name'                  =>  'Name',
            'password'              =>  'Password',
            'password_confirmation' =>  'Password Confirmation',
        ],
    ],
    'modal-confirm-delete' =>  [
        'type'      =>  [
            'success'   =>  'Success',
            'info'      =>  'Info',
            'warning'   =>  'Warning',
            'danger'    =>  'Danger',
        ],
        'title'     =>  'Are you sure?',
        'desc'      =>  'Are you sure want to delete?',
        'actions'   =>  [
            'yes'   =>  'Yes',
            'not'   =>  'Not',
        ]
    ],
    'modal-confirm-restore' =>  [
        'type'      =>  [
            'success'   =>  'Success',
            'info'      =>  'Info',
            'warning'   =>  'Warning',
            'danger'    =>  'Danger',
        ],
        'title'     =>  'Are you sure?',
        'desc'      =>  'Do you really want to reactivate this record? when doing so, it will be available and with status ACTIVE',
        'actions'   =>  [
            'yes'   =>  'Yes',
            'not'   =>  'Not',
        ]
    ]
];
