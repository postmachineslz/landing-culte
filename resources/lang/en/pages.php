<?php

return [
    'menu' => [
        'works'     => 'How it works',
        'reasons'   => 'Reasons to invest',
        'posts'     => 'News',
        'teams'     => 'Team',
        'download'  => 'Presentation',
        'about'     => 'About us',
        'contact'   => 'Contact',
        'faq-all'   => 'All',
    ],

    'works' => [
        'desc'  =>  'Cult helps the small Brazilian agribusiness to increase the efficiency of its operations through access to technical, financial and technological resources.'
    ],
    'slug'  =>  [
        'faq'   => 'faq',
    ],
    'titles' => [
        'download'              => 'Download White Paper to learn more',
        'posts'                 => 'Latest Posts',
        'post-category'         => 'News Clipping',
        'more-posts'            => 'Read more posts',
        'no-more-posts'         => 'Could not load posts',
        'our-team'              => 'Our Team',
        'learn-more-about'      => 'Learn more about Culte',
        'btn-download'          => 'I want the materials',
        'text-download'         => 'Register your email to receive the detailed presentation of our plot. The presentation will be sent in a maximum of 1 business day.',
        'address'               => 'Address',
        'get-news'              => 'Get News',
        'text-news'             => 'Enter your name and email below to receive our news first hand.',
        'btn-news'              => 'Sign-up',
        'text-copyright'        => 'Terms and Conditions I Privacy Policy I © 2018-2025 Culte Agricultural Investments. All rights reserved.',
        'watch-video'           => 'Watch Vídeo',
        'download-whitepapper'  => 'Download Whitepaper',
        'slide-to-see-more'     => 'Slide to see more',
    ],
    'contact-form' => [
        'labels' => [
            'name'      =>  'Name',
            'phone'     =>  'Phone',
            'email'     =>  'Email',
            'function'  =>  'Profession',
            'subject'   =>  'Subject',
            'message'   =>  'Message',
            'btn-text'  =>  'Send'
        ],
        'placeholders'  =>  [
            'name'      =>  'Jane Doe',
            'phone'     =>  '+xx (99) 9-9999 9999',
            'email'     =>  'janedoe@email.com',
            'function'  =>  'Web Developer',
            'subject'   =>  'Subject Here',
            'message'   =>  'Your Message here',
        ]
    ],
    'meta'  =>  [
        'subtitle'      =>  'Cryptocurrency',
        'description'   =>  'Be part of the revolution in agriculture - Sponsor projects and ideas of the fastest growing sector in the world through the field crypto'
    ],
    'countdown' => [
        'days'    =>  'Days',
        'hours'   =>  'Hours',
        'min'     =>  'Min',
        'seg'     =>  'Sec',
    ]
];
