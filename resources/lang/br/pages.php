<?php

return [
    'menu' => [
        'works'     => 'Entenda como funciona',
        'reasons'   => 'Razões para Participar',
        'posts'     => 'Notícias',
        'teams'     => 'Equipe',
        'download'  => 'White Paper',
        'about'     => 'Sobre Nós',
        'contact'   => 'Contato',
        'faq-all'   => 'Todas',
    ],

    'works' => [
        'desc'  =>  'A Culte ajuda o pequeno agro empreendedor brasileiro a aumentar a eficiência de suas operações através do acesso a recursos técnicos, financeiros e tecnológicos.'
    ],
    'slug'  =>  [
        'faq'   => 'faq',
    ],
    'titles' => [
        'download'              => 'Baixe nossa ' . '</br>' . 'apresentação explicativa',
        'posts'                 => 'Últimas Notícias',
        'post-category'         => 'Clipping de Notícis',
        'more-posts'            => 'Ler mais postagens',
        'no-more-posts'         => 'Não foi possível carregar as postagens',
        'our-team'              => 'Conheça a nossa equipe',
        'learn-more-about'      => 'Saiba mais sobre a Culte',
        'btn-download'          => 'Quero baixar o white paper',
        'text-download'         => 'Cadastre seu email para receber a apresentação detalhada da nossa plotaforma. A apresentação será enviada em no máximo 1 dia útil.',
        'address'               => 'Endereço',
        'get-news'              => 'Receba Notícias',
        'text-news'             => 'Informe seu nome e e-mail abaixo para receber nossas notícias em primeira mão.',
        'btn-news'              => 'Inscreva-se',
        'text-copyright'        => 'Termos e condições  |  Política de privacidade  |  © 2018-2025 Culte Investimentos Agrícolas. Todos os direitos reservados.',
        'watch-video'           => 'Veja o Vídeo',
        'download-whitepapper'  => 'Baixe o Whitepaper',
        'slide-to-see-more'     => 'Deslize para ver mais',
    ],
    'contact-form' => [
        'labels' => [
            'name'      =>  'Nome',
            'phone'     =>  'Telfone',
            'email'     =>  'Email',
            'function'  =>  'Profissão',
            'subject'   =>  'Assunto',
            'message'   =>  'Mensagem',
            'btn-text'  =>  'Enviar'
        ],
        'placeholders'  =>  [
            'name'      =>  'João da Silva',
            'phone'     =>  '+55 (99) 9-9999 9999',
            'email'     =>  'joao@email.com',
            'function'  =>  'Pintor',
            'subject'   =>  'Assunto',
            'message'   =>  'Sua mensagem',
        ]
    ],
    'meta'  =>  [
        'subtitle'      =>  'Criptomoeda do Campo',
        'description'   =>  'Faça parte da revolução na agricultura - Patrocine projetos e ideias do setor que mais cresce no mundo através da criptomoeda do campo'
    ],
    'countdown' => [
        'days'    =>  'Dias',
        'hours'   =>  'Horas',
        'min'     =>  'Min',
        'seg'     =>  'Seg',
    ]
];
