<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Instalador Laravel',
    'next' => 'Próximo Passo',
    'finish' => 'Instalar',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Bem-vindo ao Instalador',
        'message' => 'Bem-vindo ao assistente de configuração.',
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requisitos',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissões',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Configurações de Ambiente',
        'save' => 'Salvar .env',
        'success' => 'Suas configurações de arquivo .env foram gravadas.',
        'errors' => 'Não foi possível gravar o arquivo .env, por favor crie-o manualmente.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Terminado',
        'finished' => 'Aplicação foi instalada com sucesso',
        'exit' => 'Clique aqui para sair',
    ],

    'download' => [
        'success' => 'Obrigado pelo seu interesse na Culte Coin, em instantes enviaremos um e-mail com a nossa apresentação explicativa.',
        'wait'  =>  'Aguarde...',
        'fail'  =>  'Ocorreu um erro. Por favor, tente novamente.',
        'title_modal'   =>  'Tudo Certo',
        'close_modal'   =>  'Fechar',
    ],

    'subscriber' => [
        'success' => 'Obrigado por assinar entrar em contato, retornaremos em breve',
        'fail' => 'Ocorreu um erro. Por favor, tente novamente',
        'error' =>  'Por favor, preencha todos os campos.'
    ],

    'contact' => [
        'success' => 'Obrigado por entrar em contato, retornaremos o mais breve possível',
        'fail' => 'Ocorreu um erro. Por favor, tente novamente',
        'error' =>  'Por favor, preencha todos os campos.'
    ],
];
