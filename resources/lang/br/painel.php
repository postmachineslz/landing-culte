<?php

return [
    'menu' => [
        'banners'       =>  'Banner',
        'pages'         =>  'Páginas',
        'how_works'     =>  'Como Funciona',
        'reasons'       =>  'Razões para investir',
        'teams'         =>  'Equipe',
        'downloads'     =>  'White paper',
        'faqs'          =>  'Faq',
        'faq-category'  =>  'Categoria',
        'newsletters'   =>  'Newsletter',
        'users'         =>  'usuários',
        'list'          =>  'Listar',
        'create'        =>  'Adicioanr',
        'back'          =>  'Voltar',
        'cancel'        =>  'Cancelar',
    ],
    'titles'    =>  [
        'dashboard'     =>  'Painel',
        'my-profile'    =>  'Meu Perfil',
        'go-to-site'    =>  'Ir para o site',
        'logout'        =>  'Sair',
        'change-lang'   =>  'Change to'
    ],
    'dashboard' =>  [
        'subscribed-statistics' =>  [
            'title'     =>  'Estatísticas Incritos',
            'subtitle'  =>  'Último desempenho da campanha',
            'des1'      =>  'Inscritos',
            'des2'      =>  'Cancelados',
        ],
        'new-subscribers'       =>  [
            'title'     =>  'Novos Inscritos',
            'subtitle'  =>  'Inscritos nos últimos meses',
            'desc1'     =>  'incritos',
            'desc2'     =>  'Cancelados',
        ],
        'downloads'             =>  [
            'title'     =>  'Downloads',
            'subtitle'  =>  'Estatística de Downloads no último mês',
            'total'     =>  'Total Downloads'
        ],
        'access'  =>  [
            'title'     =>  'Acessos',
            'subtitle'  =>  'Estatísticas de acessos dos últimos 7 dias',
            'total'     =>  'TOTAL DOS ÚLTIMOS 7 DIAS',
            'desc1'     =>  'Visitas',
            'desc2'     =>  'Visualizações de página',
        ],
    ],
    'controllers'   =>  [
        'flash-messages'    =>  [
            'success' =>    [
                'create'    =>  'adicionado com sucesso',
                'edit'      =>  'alterado com sucesso',
                'destroy'   =>  'desativado com sucesso',
                'restore'   =>  'restaurado com sucesso',
            ],
            'error' =>    [
                'create'    =>  'Ocorreu um erro ao adicionar este registro. Por favor, tente novamente',
                'edit'      =>  'Ocorreu um erro ao alterar este registro. Por favor, tente novamente',
                'destroy'   =>  'Ocorreu um erro ao desativar este registro. Por favor, tente novamente',
                'restore'   =>  'Ocorreu um erro ao restaurar este registro. Por favor, tente novamente',
            ],
        ]
    ],
    'models'    =>  [
        'crud'    =>  [
            'actions'       =>  'Ações',
            'index'         =>  'Listagem de Registros',
            'show'          =>  'Detalhes',
            'create'        =>  'Adicionar',
            'edit'          =>  'Editar',
            'delete'        =>  'Remover',
            'trasheds'      =>  'Desativados',
            'restore'       =>  'Retaurar',
            'crated_at'     =>  'Criado em',
            'update_at'     =>  'Atualizado em',
            'deleted_at'    =>  'Desativado em',
            'status'    =>  [
                'active'    =>  'Ativo',
                'inactive'    =>  'Inativo',
            ]
        ],
        'banners'   =>  [
            'title'     =>  'Título',
            'desc'      =>  'Descrição',
            'image'     =>  'Imagem',
        ],
        'category_faq'  =>  [
            'name'     =>  'Categoria',
        ],
        'faqs'  =>  [
            'question'     =>  'Pergunta',
            'answer'       =>  'Resposta',
        ],
        'how_works'  =>  [
            'title'         =>  'Título',
            'image'         =>  'Imagem',
            'description'   =>  'Descrição',
        ],
        'pages'  =>  [
            'title'         =>  'Título',
            'image'         =>  'Imagem',
            'description'   =>  'Descrição',
        ],
        'reasons'  =>  [
            'title'         =>  'Título',
            'description'   =>  'Descrição',
            'image'         =>  'Imagem',
        ],
        'teams'  =>  [
            'name'          =>  'Nome',
            'function'      =>  'Profissão',
            'avatar'        =>  'Foto',
            'about'         =>  'Sobre',
        ],
        'users'  =>  [
            'name'                  =>  'Nome',
            'password'              =>  'Senha',
            'password_confirmation' =>  'Confirmação de Senha',
        ],
    ],
    'modal-confirm-delete' =>  [
        'type'      =>  [
            'success'   =>  'Sucesso',
            'info'      =>  'Info',
            'warning'   =>  'Atenção',
            'danger'    =>  'Erro',
        ],
        'title'     =>  'Você tem certeza?',
        'desc'      =>  'Tem certeza que deseja excluir?',
        'actions'   =>  [
            'yes'   =>  'Yes',
            'not'   =>  'Not',
        ]
    ],
    'modal-confirm-restore' =>  [
        'type'      =>  [
            'success'   =>  'Sucesso',
            'info'      =>  'Info',
            'warning'   =>  'Atenção',
            'danger'    =>  'Erro',
        ],
        'title'     =>  'Você tem certeza?',
        'desc'      =>  'Do you really want to reactivate this record? when doing so, it will be available and with status ACTIVE',
        'actions'   =>  [
            'yes'   =>  'Yes',
            'not'   =>  'Not',
        ]
    ]
];
