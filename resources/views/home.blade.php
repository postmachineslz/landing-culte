@extends('layouts.frontend')

@section('title', config('app.name').' | '. $titlePage)

@section('content')

    {{--HEADER--}}
    <div class="overlay d-none d-sm-block"></div>

    @foreach($banners as $banner)
        <header class="masthead text-center text-white d-flex" style="background: url({{ asset('img/uploads/banners/' . $banner->image) }}) no-repeat; background-position: center; background-size: cover ">
            <div class="container">
                <div class="row content-masthead">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <p class="name-site">Culte Coin</p>
                        <h1 class="text-left">
                            {{ $banner->title }}
                        </h1>
                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div class="socials-menu text-right d-none d-sm-block">
                            <ul class="social-btns center-block">
                                <li><button class="btn btn-facebook"><i class="fa fa-facebook pull-left" aria-hidden="true"></i></button></li>
                                <li><button class="btn btn-instagram"><i class="fa fa-instagram pull-left" aria-hidden="true"></i></button></li>
                                <li><button class="btn btn-linkedin"><i class="fa fa-linkedin pull-left" aria-hidden="true"></i></button></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <p class="text-left mb-5">
                            {{ $banner->description }}
                        </p>

                        <div class="box-buttons">
                            <a href="#" class="btn btn-default-green">
                                @lang('pages.titles.watch-video')
                            </a>

                            <a href="#" class="btn btn-default-green">
                                @lang('pages.titles.download-whitepapper')
                            </a>
                        </div>
                    </div>
                </div>

                <a href="#about" class="arrow-scroll-down">
                    <span>
                        @lang('pages.titles.slide-to-see-more')
                    </span>
                    <p>
                        <img src="{{ asset('img/arrow-circle-down.png') }}">
                    </p>
                </a>

            </div>
        </header>
    @endforeach




    {{--pattern bg--}}
    <div class="patter-bg">
        {{--<img src="{{ asset('img/pattern-bg.png') }}" class="hidden-xs img-responsive">--}}
    </div>

    {{--COUNTDOWN DESC--}}
    <div id="about" class="d-sm-block">
        <div class="container">
            <section class="content-section content-about">
                <h3>{{ $page['title'] }}</h3>
                <p>{{ $page['desc'] }}</p>

                <a class="js-scroll-trigger" href="{{ route('site') }}#download">
                    @lang('pages.titles.download')
                </a>
            </section>
        </div>{{-- container --}}
    </div>{{--about--}}

    {{--HOT IT WORKS--}}
    <section id="works">
        <img src="{{ asset('img/world_map.svg') }}" alt="" class="bg-word_map">
        <div class="container">
            <div class="box-works">

                <div class="desc-works content-section">
                    <h2 class="section-heading">
                        @lang('pages.menu.works')
                    </h2>
                    <h5>
                        @lang('pages.works.desc')
                    </h5>
                    <a class="js-scroll-trigger download-link" href="{{ route('site') }}#download">
                        @lang('pages.titles.download')
                    </a>
                </div>

                <div class="row section-works">
                    @php $i = 1; @endphp
                    @foreach($works as $work)
                        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12 item-work">
                            <div class="row">
                                <div class="col-xs-3 col-sm-2 col-md-2 col-lg-2">
                                    <span class="number-work">@if($i < 9) 0{{ $i }} @else {{ $i }} @endif</span>
                                </div>
                                <div class="col-xs-9 col-sm-10 col-md-10 col-lg-10">
                                    <div class="line-divider">
                                        <h3>{{ $work->title }}</h3>
                                    </div>
                                    <p>{{ $work->description }}.</p>
                                </div>
                            </div>
                        </div>{{--.col--}}
                        @php $i++; @endphp
                    @endforeach
                </div>{{--.row--}}
            </div>{{--.box-works--}}
        </div>{{--.container--}}
    </section>

    {{-- REASONS TO INVEST --}}
    <section id="reasons">
        <div class="container">
            <div class="box-banner content-section">

                <h2>@lang('pages.menu.reasons')</h2>

                <div class="carousel slide carousel-fade" data-ride="carousel" id="reason-carousel">
                    <div class="carousel-inner">
                        @php $i = 1; @endphp
                        @foreach($reasons as $reason)
                            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                            <blockquote>
                                <div class="row">

                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <img class="img-responsive img-reason" src="{{ asset('img/uploads/reasons/' . $reason->image) }}">
                                    </div>{{--col-6--}}

                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 details-reason">
                                        <h4 class="number">
                                            @if($i < 9) 0{{ $i }} @else {{ $i }} @endif
                                        </h4>

                                        <div class="desc-itens">
                                            <h6 class="title-banner">{{ $reason->title }}</h6>

                                            <a class="d-none d-sm-block js-scroll-trigger download-link" href="{{ route('site') }}#download">
                                                @lang('pages.titles.download')

                                            </a>
                                            <p class="desc-banner">{{ $reason->description }}</p>

                                            <a class="visible-xs d-block d-sm-none js-scroll-trigger download-link" href="{{ route('site') }}#download">
                                                @lang('pages.titles.download')
                                            </a>

                                            <div id="control-slider">
                                                <a data-slide="prev" href="#reason-carousel" class="left carousel-control">
                                                    <img src="{{ asset('img/arrow-circle-left.png') }}"  aria-hidden="true">
                                                </a>
                                                <a data-slide="next" href="#reason-carousel" class="right carousel-control">
                                                    <img src="{{ asset('img/arrow-circle-right.png') }}"  aria-hidden="true">
                                                </a>
                                            </div>
                                        </div>
                                    </div>{{--col-6 details-reason--}}
                                </div>
                            </blockquote>
                        </div>
                        @php $i++; @endphp
                        @endforeach
                    </div>
                </div>{{-- carousel slide --}}
                <ol class="carousel-indicators">
                    @php $i = 0; @endphp
                    @foreach($reasons as $reason)
                        <li data-target="#reason-carousel" data-slide-to="{{ $i }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                    @php $i++; @endphp
                    @endforeach
                </ol>
            </div>{{--box-banner--}}
        </div>
    </section>


    {{--TEAMS--}}
    <section id="teams">
        <div class="container">
            <div class="box-teams">
                <h2 class="section-heading">
                    @lang('pages.titles.our-team')
                </h2>
            </div>
        </div>{{--.container--}}

        <div class="container">
            <style type="text/css" id="slider-css"></style>

            <div class="spe-cor">
                <div class="row">
                    <div id="slider-2" class="carousel carousel-by-item slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @php $i = 0; @endphp
                            @foreach($teams as $team)
                                <div class="carousel-item @php if ($i==0) {echo 'active';} @endphp">
                                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                        <div class="list-team @php if ($i%2) {echo 'list-team-even';} @endphp">
                                            <img src="{{ asset('/img/uploads/teams/' . $team->avatar) }}" class="img-responsive avatar-team">
                                            <span class="vertical-line">
                                                <img src="{{ asset('img/vertical-line.png') }}" class="img-responsive">
                                            </span>
                                            <h3>{{ $team->name }}</h3>
                                            <h4>{{ $team->function }}</h4>
                                            <p>{{ $team->about }}</p>

                                            <div class="team-social-links">
                                                <a href="{{ $team->facebook }}" target="_blank"><i class="fa fa-facebook-f"></i></a>
                                                <a href="{{ $team->instagram }}" target="_blank"><i class="fa fa-instagram"></i></a>
                                                <a href="{{ $team->linkedin }}" target="_blank"><i class="fa fa-linkedin"></i></a>
                                            </div>
                                        </div>{{--.list-team--}}
                                    </div>{{--col-3--}}
                                </div>
                                @php $i++; @endphp
                            @endforeach
                        </div>

                        <div class="carousel-navigation">
                            <a class="carousel-control-prev" href="#slider-2" role="button" data-slide="prev">
                                <img src="{{ asset('img/arrow-circle-left.png') }}" aria-hidden="true">
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#slider-2" role="button" data-slide="next">
                                <img src="{{ asset('img/arrow-circle-right.png') }}"  aria-hidden="true">
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>{{--.container--}}

    </section>

    {{--POSTS--}}
    <section id="posts">
        <div class="container">
            <div class="box-posts">
                <h2 class="section-heading">
                    @lang('pages.titles.posts')
                </h2>

                <div class="row">
                    @if($posts)
                        @php $i = 0; @endphp
                        @foreach($posts as $post)
                            @if($i===3) @php break; @endphp @endif
                            <div class="col-6 col-md-4">
                                <div class="post-item shadow">
                                    <div class="post-img">
                                        <img src="<?php /* echo $post->children('http://purl.org/rss/1.0/modules/content/')->encoded; */?>" alt="">
                                        <img class="img-responsive" src="https://cdn-images-1.medium.com/max/300/{{ $post->virtuals->previewImage->imageId }}">
                                    </div>

                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="https://medium.com/culte/{{ $post->uniqueSlug }}" target="_blank">
                                                {{ str_limit($post->title, 420, ' (...)')}}
                                            </a>
                                        </h3>

                                        <div class="post-category">
                                            <a href="#">
                                                @lang('pages.titles.post-category')
                                            </a>
                                            <span class="pull-right">
                                                <div class="share-post">
                                                    <i class="fa fa-share-alt"></i>
                                                    <div id="links-share">
                                                        <a OnClick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250'); return false;" href="https://www.facebook.com/sharer/sharer.php?u=https://medium.com/culte/{{ $post->uniqueSlug }}">
                                                            <i class="fa fa-facebook-f fa-lg"></i>
                                                        </a>
                                                        <a OnClick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250'); return false;" href="https://twitter.com/home?status=https://medium.com/culte/{{ $post->uniqueSlug }}">
                                                            <i class="fa fa-twitter fa-lg"></i>
                                                        </a>
                                                        <a OnClick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250'); return false;" href="https://www.linkedin.com/shareArticle?mini=true&url=https://medium.com/culte/{{ $post->uniqueSlug }}&title={{ str_limit($post->title,250,'...') }}&source=LinkedIn">
                                                            <i class="fa fa-linkedin fa-lg"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>{{--.post-item--}}
                            </div>{{--.col-4--}}
                            @php $i++; @endphp
                        @endforeach
                        <div class="more-posts">
                            <a href="https://medium.com/culte" target="_blank">
                                @lang('pages.titles.more-posts')
                            </a>
                        </div>
                    @else
                        <p class="text-muted small p-lg-3">
                            @lang('pages.titles.no-more-posts')
                        </p>
                    @endif
                </div>{{--.row--}}
            </div>{{--.box-posts--}}
        </div>{{--.container--}}
    </section>

    {{--DOWNLOAD--}}
    @include('_partials.frontend.download')

    {{--FOOTER--}}
    @include('_partials.frontend.footer')

@endsection

@push('scripts')
    <script type="text/javascript">
        function GetUnique(e){var l=[],s=temp_c=[],t=["col-md-1","col-md-2","col-md-3","col-md-4","col-md-6","col-md-12","col-sm-1","col-sm-2","col-sm-3","col-sm-4","col-sm-6","col-sm-12","col-lg-1","col-lg-2","col-lg-3","col-lg-4","col-lg-6","col-lg-12","col-xs-1","col-xs-2","col-xs-3","col-xs-4","col-xs-6","col-xs-12","col-xl-1","col-xl-2","col-xl-3","col-xl-4","col-xl-6","col-xl-12"];$(e).each(function(){for(var l=$(e+" > div").attr("class").split(/\s+/),t=0;t<l.length;t++)s.push(l[t])});for(var c=0;c<s.length;c++)temp_c=s[c].split("-"),2==temp_c.length&&(temp_c.push(""),temp_c[2]=temp_c[1],temp_c[1]="xs",s[c]=temp_c.join("-")),-1==$.inArray(s[c],l)&&$.inArray(s[c],t)&&l.push(s[c]);return l}function setcss(e,l,s){for(var t=["","","","","",""],c=d=f=g=0,r=[1200,992,768,567,0],a="",o=[],a=0;a<e.length;a++){var i=e[a].split("-");if(3==i.length){switch(i[1]){case"xl":d=0;break;case"lg":d=1;break;case"md":d=2;break;case"sm":d=3;break;case"xs":d=4}t[d]=i[2]}}for(var n=0;n<t.length;n++)if(""!=t[n]){if(0==c&&(c=12/t[n]),f=12/t[n],g=100/f,a=s+" > .carousel-item.active.carousel-item-right,"+s+" > .carousel-item.carousel-item-next {-webkit-transform: translate3d("+g+"%, 0, 0);transform: translate3d("+g+", 0, 0);left: 0;}"+s+" > .carousel-item.active.carousel-item-left,"+s+" > .carousel-item.carousel-item-prev {-webkit-transform: translate3d(-"+g+"%, 0, 0);transform: translate3d(-"+g+"%, 0, 0);left: 0;}"+s+" > .carousel-item.carousel-item-left, "+s+" > .carousel-item.carousel-item-prev.carousel-item-right, "+s+" > .carousel-item.active {-webkit-transform: translate3d(0, 0, 0);transform: translate3d(0, 0, 0);left: 0;}",f>1){for(k=0;k<f-1;k++)o.push(l+" .cloneditem-"+k);o.length&&(a=a+o.join(",")+"{display: block;}"),o=[]}0!=r[n]&&(a="@media all and (min-width: "+r[n]+"px) and (transform-3d), all and (min-width:"+r[n]+"px) and (-webkit-transform-3d) {"+a+"}"),$("#slider-css").prepend(a)}$(l).each(function(){for(var e=$(this),l=0;l<c-1;l++)(e=e.next()).length||(e=$(this).siblings(":first")),e.children(":first-child").clone().addClass("cloneditem-"+l).appendTo($(this))})}

        //Use Different Slider IDs for multiple slider
        $(document).ready(function() {
            var item = '#slider-1 .carousel-item';
            var item_inner = "#slider-1 .carousel-inner";
            classes = GetUnique(item);
            setcss(classes, item, item_inner);


            var item_1 = '#slider-2 .carousel-item';
            var item_inner_1 = "#slider-2 .carousel-inner";
            classes = GetUnique(item_1);
            setcss(classes, item_1, item_inner_1);
        });
    </script>
@endpush

