@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        {!! Form::model($faq, [
                            'method'    => 'PUT',
                            'route'     => ['faq.update', $faq->id],
                            'class'     => 'form-horizontal'
                        ]) !!}

                        @include('painel.faqs._form')

                        {!! Form::close() !!}

                    </div>{{--.col-8--}}
                </div>{{--.row--}}
            </div>{{--container-fluid--}}
        </div>{{--/content--}}
    </div>{{--main-panel--}}
@endsection
