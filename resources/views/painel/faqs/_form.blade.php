<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <div class="card">
            <div class="header">
                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                    <i class="fa fa-arrow-left fa-lg"></i>
                    @lang('painel.menu.back')
                </a>
                <legend>{{ $faq->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}</legend>
            </div>
            <div class="content">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('question', trans('painel.models.faqs.question')) !!}
                                {!! Form::text('question', null, ['class' => 'form-control', 'placeholder' => trans('painel.models.faqs.question')]) !!}
                                @if($errors->has('question'))
                                    <span class="help-block">{{ $errors->first('question') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group {{ $errors->has('answer') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('answer', trans('painel.models.faqs.answer')) !!}
                                {!! Form::text('answer', null, ['class' => 'form-control', 'placeholder' => trans('painel.models.faqs.answer')]) !!}
                                @if($errors->has('answer'))
                                    <span class="help-block">{{ $errors->first('answer') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group {{ $errors->has('category_faq_id') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('category_faq_id', trans('painel.menu.faq-category')) !!}
                                {!! Form::select('category_faq_id', $categories, old('category_faq_id'), ['class' => 'form-control', 'placeholder' => trans('painel.menu.faq-category')]) !!}

                                @if($errors->has('category_faq_id'))
                                    <span class="help-block">{{ $errors->first('category_faq_id') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- end content-->
        </div><!--  end card  -->

    </div>{{--.col-9--}}

    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="card">
            <div class="content">
                <div class="text-center">
                    <h5>@lang('painel.models.crud.actions')</h5>
                    <hr>
                    <button type="submit" class="btn btn-sm btn-fill btn-info"><i class="fa fa-save"></i>
                        {{ $faq->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}
                    </button>
                    <a href="{{ route('faq.index') }}" class="btn btn-sm btn-fill btn-warning">
                        <i class="fa fa-exclamation-triangle"></i>
                        @lang('painel.menu.cancel')
                    </a>
                </div>
            </div>
        </div><!--  end card  -->
    </div>{{--.col-3--}}

</div>{{--.row--}}

