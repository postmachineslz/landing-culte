@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        {!! Form::model($team, [
                            'method'    => 'PUT',
                            'route'     => ['teams.update', $team->id],
                            'class'     => 'form-horizontal',
                            'enctype'   => 'multipart/form-data'
                        ]) !!}

                        @include('painel.teams._form')

                        {!! Form::close() !!}

                    </div>{{--.col-8--}}
                </div>{{--.row--}}
            </div>{{--container-fluid--}}
        </div>{{--/content--}}
    </div>{{--main-panel--}}
@endsection
