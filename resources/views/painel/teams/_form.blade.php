<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <div class="card">
            <div class="header">
                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                    <i class="fa fa-arrow-left fa-lg"></i> @lang('painel.menu.back')
                </a>
                <legend>{{ $team->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}</legend>
            </div>
            <div class="content">

                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('name', trans('painel.models.teams.name')) !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('painel.models.teams.name')]) !!}
                                @if($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div class="form-group {{ $errors->has('function') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('function', trans('painel.models.teams.function')) !!}
                                {!! Form::text('function', null, ['class' => 'form-control', 'placeholder' => trans('painel.models.teams.function')]) !!}
                                @if($errors->has('function'))
                                    <span class="help-block">{{ $errors->first('function') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group {{ $errors->has('facebook') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('facebook', 'Facebook') !!}
                                {!! Form::text('facebook', null, ['class' => 'form-control', 'placeholder' => 'https://facebook.com/joaodasilva']) !!}
                                @if($errors->has('facebook'))
                                    <span class="help-block">{{ $errors->first('facebook') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group {{ $errors->has('instagram') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('instagram', 'Instagram') !!}
                                {!! Form::text('instagram', null, ['class' => 'form-control', 'placeholder' => 'https://instagram.com/joaodasilva']) !!}
                                @if($errors->has('instagram'))
                                    <span class="help-block">{{ $errors->first('instagram') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group {{ $errors->has('linkedin') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('linkedin', 'Linkedin') !!}
                                {!! Form::text('linkedin', null, ['class' => 'form-control', 'placeholder' => 'https://linkedin.com/joaodasilva']) !!}
                                @if($errors->has('linkedin'))
                                    <span class="help-block">{{ $errors->first('linkedin') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('avatar', trans('painel.models.teams.avatar')) !!}
                                {!! Form::file('avatar', null, ['class' => 'form-control', 'placeholder' => trans('painel.models.teams.avatar')]) !!}
                                @if($errors->has('avatar'))
                                    <span class="help-block">{{ $errors->first('avatar') }}</span>
                                @endif
                                @if($team->avatar)
                                    <br>
                                    <img src="{{ asset('/img/uploads/teams/' . $team->avatar) }}" width="200">
                                @endif
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group {{ $errors->has('about') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('about', trans('painel.models.teams.about')) !!}
                                {!! Form::textarea('about', null, ['class' => 'form-control', 'rows' => 4, 'placeholder' => trans('painel.models.teams.about')]) !!}
                                @if($errors->has('about'))
                                    <span class="help-block">{{ $errors->first('about') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- end content-->
        </div><!--  end card  -->

    </div>{{--.col-9--}}

    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="card">
            <div class="content">
                <div class="text-center">
                    <h5>@lang('painel.models.crud.actions')</h5>
                    <hr>
                    <button type="submit" class="btn btn-sm btn-fill btn-info"><i class="fa fa-save"></i>
                        {{ $team->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}
                    </button>
                    <a href="{{ route('teams.index') }}" class="btn btn-sm btn-fill btn-warning">
                        <i class="fa fa-exclamation-triangle"></i> 
                        @lang('painel.menu.cancel')
                    </a>
                </div>
            </div>
        </div><!--  end card  -->
    </div>{{--.col-3--}}

</div>{{--.row--}}

