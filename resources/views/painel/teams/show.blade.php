@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')
    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="content">
                                <a href="{{ route('teams.index') }}" class="btn btn-info btn-fill btn-wd btn-add pull-right">
                                    <i class="fa fa-arrow-left fa-lg"></i>
                                    @lang('painel.menu.back')
                                </a>
                                <h3><strong>@lang('painel.models.teams.name'): </strong> {{ $team->name }}</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <p><strong>@lang('painel.models.teams.function'): </strong> {{ $team->function }}</p>
                                        <p><strong>Facebook: </strong> {{ $team->facebook }}</p>
                                        <p><strong>Instagram: </strong> {{ $team->instagram }}</p>
                                        <p><strong>Linkedin: </strong> {{ $team->linkedin }}</p>
                                        <p><strong>@lang('painel.models.teams.about'): </strong>{{ $team->about }}</p>
                                        <p><strong>@lang('painel.models.crud.crated_at'): </strong>{{ $team->created_at }}</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        @if($team->avatar)
                                            <img src="{{ asset('/img/uploads/teams/' . $team->avatar) }}" class="img-responsive">
                                        @else
                                            <img src="{{ asset('/img/default-image.png') }}" class="img-responsive">
                                        @endif
                                    </div>
                                </div>

                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection

