<div class="fresh-datatables">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('painel.models.reasons.title')</th>
                <th>@lang('painel.models.reasons.image')</th>
                <th class="text-right">@lang('painel.models.crud.actions')</th>
            </tr>
        </thead>
        <tbody>
            @foreach($reasons as $reason)
                <tr>
                    <td>{{ $reason->id }}</td>
                    <td>{{ $reason->title }}</td>
                    <td>
                        @if($reason->image)
                            <img src="{{ asset('/img/uploads/reasons/' . $reason->image) }}" width="100">
                        @else
                            <img src="{{ asset('/img/default-image.png') }}" width="100">
                        @endif
                    </td>
                    <td class="text-right">
                        <a href="{{ route('reasons.show', $reason->id) }}" class="btn btn-primary btn-sm btn-fill" title="@lang('painel.models.crud.show')" data-tooltip="tooltip">
                            <i class="fa fa-eye"></i>
                        </a>

                        @if($reason->deleted_at == null)
                            <a href="{{ route('reasons.edit', $reason->id) }}" class="btn btn-info btn-sm btn-fill" title="@lang('painel.models.crud.edit')" data-tooltip="tooltip">
                                <i class="fa fa-pencil"></i>
                            </a>
                        @endif

                        @if($reason->deleted_at !== null)
                            {!! Form::open(['route' => ['reasons.restore', $reason->id], 'method' => 'post', 'class' => 'pull-right', 'id' => 'restore-register']) !!}
                            <button type="submit" class="btn btn-fill btn-success btn-sm delete-btn" title="@lang('painel.models.crud.restore')" data-tooltip="tooltip">
                                <i class="fa fa-refresh"></i>
                            </button>
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route' => ['reasons.destroy', $reason->id], 'method' => 'delete', 'class' => 'delete_form pull-right', 'id' => 'delete-btn']) !!}
                            <button type="submit" class="btn btn-fill btn-danger btn-sm delete-btn" title="@lang('painel.models.crud.delete')" data-tooltip="tooltip">
                                <i class="fa fa-times"></i>
                            </button>
                            {!! Form::close() !!}
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>