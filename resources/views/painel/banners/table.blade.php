<div class="fresh-datatables">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('painel.models.banners.title')</th>
            <th>Status</th>
            <th>@lang('painel.models.banners.image')</th>
            <th class="text-right">@lang('painel.models.crud.actions')</th>
        </tr>
        </thead>

        <tbody>
            @foreach($banners as $banner)
                <tr>
                    <td>{{ $banner->id }}</td>
                    <td>{{ $banner->title }}</td>
                    <td>{!! $banner::getStatus($banner->status, true) !!}</td>
                    <td>
                        @if($banner->image)
                            <img src="{{ asset('/img/uploads/banners/' . $banner->image) }}" width="100">
                        @else
                            <img src="{{ asset('/img/default-image.png') }}" width="80">
                        @endif
                    </td>

                    <td class="text-right">
                        <a href="{{ route('banners.show', ['id' => $banner->id]) }}" class="btn btn-fill btn-info btn-sm view" data-toggle="tooltip" title="@lang('painel.models.crud.show')">
                            <i class="fa fa-eye"></i>
                        </a>

                        <a href="{{ route('banners.edit', $banner->id) }}" class="btn btn-warning btn-sm btn-fill" data-toggle="tooltip" title="@lang('painel.models.crud.edit')">
                            <i class="fa fa-pencil"></i>
                        </a>

                        {!! Form::open(['route' => ['banners.destroy', $banner->id], 'method' => 'delete', 'class' => 'delete_form pull-right', 'id' => 'delete-btn']) !!}
                            <button type="submit" class="btn btn-fill btn-danger btn-sm delete-btn" data-toggle="tooltip" title="@lang('painel.models.crud.delete')">
                                <i class="fa fa-times"></i>
                            </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>