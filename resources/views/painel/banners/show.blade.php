@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')
    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="content">
                                <a href="{{ route('banners.index') }}" class="btn btn-info btn-fill btn-wd btn-add pull-right">
                                    <i class="fa fa-arrow-left fa-lg"></i> @lang('painel.menu.back')
                                </a>
                                <h3><strong>@lang('painel.models.banners.title'): </strong> {{ $banner->title }}</h3>
                                <hr>
                                <p><strong>Status: </strong>{!! $banner->getStatus($banner->status) !!}</p>
                                <p>
                                    <strong>@lang('painel.models.banners.desc'):</strong>
                                    {{ $banner->description }}
                                </p>
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection

