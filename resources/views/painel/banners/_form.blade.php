<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="card">
            <div class="header">
                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                    <i class="fa fa-arrow-left fa-lg"></i> 
                    @lang('painel.menu.back')
                </a>
                <legend>{{ $banner->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}</legend>
            </div>

            <div class="content">

                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        {!! Form::label('title', trans('painel.models.banners.title')) !!}
                        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => trans('painel.models.banners.title')]) !!}
                        @if($errors->has('title'))
                            <span class="help-block">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>

                {{-- STATUS --}}
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        {!! Form::label('status', trans('painel.models.crud.status.active')) !!}
                        <br>
                        <div class="switch" data-on-label="" data-off-label="">
                            {{ Form::checkbox('status', true) }}
                        </div>
                        @if($errors->has('status'))
                            <span class="help-block">{{ $errors->first('status') }}</span>
                        @endif
                    </div>
                </div>

                {{-- IMAGE --}}
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        {!! Form::label('image', trans('painel.models.banners.image')) !!}
                        {!! Form::file('image') !!}
                        @if($errors->has('title'))
                            <span class="help-block">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>

                {{-- DESCRIPTION --}}
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {!! Form::label('description', trans('painel.models.banners.desc')) !!}
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'rows' => '4', 'placeholder' => trans('painel.models.banners.desc') ]) !!}
                        @if($errors->has('description'))
                            <span class="help-block">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>

            </div><!-- end content-->
        </div><!--  end card  -->

    </div>{{--.col-9--}}

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <div class="card">
            <div class="content">
                <div class="text-center">
                    <h5>@lang('painel.models.crud.actions')</h5>
                    <hr>
                    <button type="submit" class="btn btn-xs btn-fill btn-info"><i class="fa fa-save"></i>
                        {{ $banner->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}
                    </button>
                    <a href="{{ route('banners.index') }}" class="btn btn-xs btn-fill btn-warning">
                        <i class="fa fa-exclamation-triangle"></i> @lang('painel.menu.cancel')
                    </a>
                </div>
            </div>
        </div><!--  end card  -->
    </div>{{--.col-3--}}

</div>{{--.row--}}

