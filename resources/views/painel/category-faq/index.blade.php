@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <a href="{{ route('category-faq.create') }}" class="pull-right btn btn-default btn-xs btn-fill">
                                    <i class="fa fa-plus fa-lg"></i>
                                    @lang('painel.models.crud.create')
                                </a>
                                <legend>
                                    @lang('painel.menu.faq-category')
                                </legend>
                            </div>
                            <div class="content">
                                @include('painel.category-faq.table')
                                <hr>
                                <div class="text-center">
                                    {{ $categories->appends( request()->query())->render() }}
                                </div>
                                <div class="text-right">
                                    <small>{{ $categories->count() }} {{ str_plural(trans('painel.menu.faq-category'), $categories->count()) }}</small>
                                </div>
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection
