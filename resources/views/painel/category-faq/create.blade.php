@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        {!! Form::model($category, [
                           'method'    => 'POST',
                           'route'     => 'category-faq.store',
                           'class'     => 'form-horizontal'
                        ]) !!}

                        @include('painel.category-faq._form')

                        {!! Form::close() !!}

                    </div>{{--.col-8--}}
                </div>{{--.row--}}
            </div>{{--container-fluid--}}
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection