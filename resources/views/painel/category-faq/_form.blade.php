<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <div class="card">
            <div class="header">
                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                    <i class="fa fa-arrow-left fa-lg"></i>
                    @lang('painel.menu.back')
                </a>
                <legend>{{ $category->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}</legend>
            </div>
            <div class="content">

                <div class="row">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('name', trans('painel.menu.faq-category')) !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('painel.menu.faq-category')]) !!}
                                @if($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    {{-- STATUS --}}
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                            {!! Form::label('status', trans('painel.models.crud.status.active')) !!}
                            <br>
                            <div class="switch" data-on-label="" data-off-label="">
                                {{ Form::checkbox('status', true) }}
                            </div>
                            @if($errors->has('status'))
                                <span class="help-block">{{ $errors->first('status') }}</span>
                            @endif
                        </div>
                    </div>

                </div>

            </div><!-- end content-->
        </div><!--  end card  -->

    </div>{{--.col-9--}}

    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="card">
            <div class="content">
                <div class="text-center">
                    <h5>@lang('painel.models.crud.actions')</h5>
                    <hr>
                    <button type="submit" class="btn btn-sm btn-fill btn-info"><i class="fa fa-save"></i>
                        {{ $category->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}
                    </button>
                    <a href="{{ route('category-faq.index') }}" class="btn btn-sm btn-fill btn-warning">
                        <i class="fa fa-exclamation-triangle"></i>
                        @lang('painel.menu.cancel')
                    </a>
                </div>
            </div>
        </div><!--  end card  -->
    </div>{{--.col-3--}}

</div>{{--.row--}}

