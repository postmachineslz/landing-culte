<div class="fresh-datatables">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('painel.menu.faq-category')</th>
                <th>Slug</th>
                <th class="text-right">@lang('painel.models.crud.actions')</th>
            </tr>
        </thead>

        <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->slug }}</td>
                    <td>{!! $category::getStatus($category->status, true) !!}</td>

                    <td class="text-right">
                        <a href="{{ route('category-faq.show', $category->id) }}" class="btn btn-info btn-sm btn-fill" title="@lang('painel.models.crud.show')" data-tooltip="tooltip">
                            <i class="fa fa-eye"></i>
                        </a>

                        <a href="{{ route('category-faq.edit', $category->id) }}" class="btn btn-warning btn-sm btn-fill" title="@lang('painel.models.crud.edit')" data-tooltip="tooltip">
                            <i class="fa fa-pencil"></i>
                        </a>

                        {!! Form::open(['route' => ['category-faq.destroy', $category->id], 'method' => 'delete', 'class' => 'delete_form pull-right', 'id' => 'delete-btn']) !!}
                            <button type="submit" class="btn btn-fill btn-danger btn-sm delete-btn" title="@lang('painel.models.crud.delete')" data-tooltip="tooltip">
                                <i class="fa fa-times"></i>
                            </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>