<div class="fresh-datatables">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Criação</th>
                <th class="text-center">Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at }}</td>

                    <td class="text-right">
                        <a href="{{ route('users.show', $user->id) }}" class="btn btn-primary btn-sm btn-fill" title="Visualizar" data-tooltip="tooltip">
                            <i class="fa fa-eye"></i>
                        </a>

                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info btn-sm btn-fill" title="Editar" data-tooltip="tooltip">
                            <i class="fa fa-pencil"></i>
                        </a>

                        {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete', 'class' => 'delete_form pull-right', 'id' => 'delete-btn']) !!}
                            <button type="submit" class="btn btn-fill btn-danger btn-sm delete-btn" title="Remover" data-tooltip="tooltip">
                                <i class="fa fa-times"></i>
                            </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>