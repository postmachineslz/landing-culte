@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <a href="{{ route('users.trashed') }}" class="pull-right btn btn-warning btn-xs btn-fill">
                                    <i class="fa fa-trash-o fa-lg"></i> Removidos
                                </a>

                                <a href="{{ route('users.create') }}" class="pull-right btn btn-default btn-xs btn-fill">
                                    <i class="fa fa-plus fa-lg"></i> Adicionar
                                </a>
                                <legend>Listagem de Usuários</legend>
                            </div>
                            <div class="content">
                                @include('painel.users.table')
                                <hr>
                                <div class="text-center">
                                    {{ $users->appends( request()->query())->render() }}
                                </div>
                                <div class="text-right">
                                    <small>{{ $users->count() }} {{ str_plural('Usuário', $users->count()) }}</small>
                                </div>
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->
            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}
@endsection

