<div class="fresh-datatables">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th class="disabled-sorting text-right">Ações</th>
            </tr>
        </thead>

        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>

                    <td class="text-right">
                        <a href="{{ route('users.show', ['id' => $user->id]) }}" class="btn btn-fill btn-info btn-sm view" data-toggle="tooltip" title="Detalhes">
                            <i class="fa fa-eye"></i>
                        </a>

                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning btn-sm btn-fill" data-toggle="tooltip" title="Editar">
                            <i class="fa fa-pencil"></i>
                        </a>

                        {!! Form::open(['route' => ['users.forceDelete', $user->id], 'method' => 'post', 'class' => 'pull-right', 'id' => 'restore-user']) !!}
                            <button type="submit" class="btn btn-fill btn-danger btn-sm delete-btn" data-toggle="tooltip" title="Remover permanentimente">
                                <i class="fa fa-trash"></i>
                            </button>
                        {!! Form::close() !!}

                        {!! Form::open(['route' => ['users.restore', $user->id], 'method' => 'post', 'class' => 'pull-right', 'id' => 'restore-user']) !!}
                            <button type="submit" class="btn btn-fill btn-success btn-sm delete-btn" data-toggle="tooltip" title="Restaurar">
                                <i class="fa fa-refresh"></i>
                            </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>