<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <div class="card">
            <div class="header">
                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                    <i class="fa fa-arrow-left fa-lg"></i> Voltar
                </a>
                <legend>{{ $user->exists ? 'Editar' : 'Adicionar' }}</legend>
            </div>
            <div class="content">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('name', 'Nome Completo') !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome Completo']) !!}
                                @if($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                                @if($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    @if(!$user->exists)
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    {!! Form::label('password', 'Senha') !!}
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Senha">
                                    @if($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <label for="password-confirm">Confirmação de Senha</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirmação de Senha">
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->

    </div>{{--.col-9--}}

    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="card">
            <div class="content">
                <div class="text-center">
                    <h5>Ações</h5>
                    <hr>
                    <button type="submit" class="btn btn-sm btn-fill btn-info"><i class="fa fa-save"></i>
                        {{ $user->exists ? 'Editar' : 'Salvar' }}
                    </button>
                    <a href="{{ route('users.index') }}" class="btn btn-sm btn-fill btn-warning"><i class="fa fa-exclamation-triangle"></i> Cancelar</a>
                </div>
            </div>
        </div><!--  end card  -->
    </div>{{--.col-3--}}

</div>{{--.row--}}

