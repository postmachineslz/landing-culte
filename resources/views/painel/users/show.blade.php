@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')
    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="content">
                                <a href="{{ route('users.index') }}" class="btn btn-info btn-fill btn-wd btn-add pull-right">
                                    <i class="fa fa-arrow-left fa-lg"></i> Voltar
                                </a>
                                <h3><strong>Nome: </strong> {{ $user->name }}</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <p><strong>Email: </strong>{{ $user->email }}</p>
                                        <p><strong>Cadastrado em: </strong>{{ $user->created_at }}</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <img src="{{ asset('/img/default-image.png') }}" class="img-responsive img-circle">
                                    </div>
                                </div>

                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection

