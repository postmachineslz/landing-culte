@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">

                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="card">
                            <div class="header">
                                <h3 class="text-left">{{ $user->name }} - Alterar Senha</h3>
                                <hr>
                            </div>
                            <div class="content">
                                {!! Form::open(['route' => 'painel.profile.password.update']) !!}

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Senha</label>
                                    <input id="password" type="password" class="form-control" placeholder="Senha" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm">Confirmar Senha</label>
                                    <input id="password-confirm" type="password" class="form-control" placeholder="Confirmar Senha" name="password_confirmation" required>
                                </div>

                            </div>{{--content--}}
                        </div>{{--card--}}
                    </div>{{--col-8--}}

                    {{--sidebar--}}
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="{{ asset('painel/img/full-screen-image-4.jpg') }}" />
                            </div>
                            <div class="content">
                                <div class="author">
                                    <img class="avatar border-gray" src="{{ asset('painel/img/default-avatar.png') }}" />
                                    <p>{{ $user->email }}</p>
                                    <hr>

                                    <p class="small"><strong>Data de criação: </strong>{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y') }}</p>
                                    <p class="small"><strong>Alterado pela última vez em: </strong>{{ \Carbon\Carbon::parse($user->updated_at)->format('d/m/Y') }}</p>
                                </div>
                                <hr>
                                <br>
                                <button type="submit" class="btn btn-block btn-success btn-fill">
                                    Alterar Senha
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>{{--.row--}}
            </div>
        </div>
    </div>

@endsection
