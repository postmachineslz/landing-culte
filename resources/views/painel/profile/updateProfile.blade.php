@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    {!! Form::model($user, ['route'     => ['storeUpdateProfile']]) !!}
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title text-left">{{ $titlePage }}</h4>
                                <hr>
                            </div>
                            <div class="content">

                                <input type="hidden" value="{{ $user->email }}" name="email">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            {!! Form::label('name', 'Nome') !!}
                                            <input type="text" name="name" value="{{ $user->name }}" class="form-control">
                                            @if($errors->has('name'))
                                                <span class="help-block">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }}">
                                            <label for="name">CPF</label>
                                            <input id="cpf" type="text" class="form-control" name="cpf" placeholder="CPF" value="{{ auth()->user()->cpf }}" disabled="disabled">

                                            @if ($errors->has('cpf'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('cpf') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>{{--row--}}

                                <div class="row">
                                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                        <div class="form-group {{ $errors->has('street') ? 'has-error' : '' }}">
                                            {!! Form::label('street', 'Rua') !!}
                                            {!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Rua']) !!}
                                            @if($errors->has('street'))
                                                <span class="help-block">{{ $errors->first('street') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="form-group {{ $errors->has('number') ? 'has-error' : '' }}">
                                            {!! Form::label('number', 'Número') !!}
                                            {!! Form::text('number', null, ['class' => 'form-control', 'placeholder' => 'Número']) !!}
                                            @if($errors->has('number'))
                                                <span class="help-block">{{ $errors->first('number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>{{--row--}}

                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                        <div class="form-group {{ $errors->has('district') ? 'has-error' : '' }}">
                                            {!! Form::label('district', 'Bairro') !!}
                                            {!! Form::text('district', null, ['class' => 'form-control', 'placeholder' => 'Bairro']) !!}
                                            @if($errors->has('district'))
                                                <span class="help-block">{{ $errors->first('district') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group {{ $errors->has('postal_code') ? 'has-error' : '' }}">
                                            {!! Form::label('postal_code', 'Código Postal') !!}
                                            {!! Form::text('postal_code', null, ['class' => 'form-control', 'placeholder' => 'Código Postal']) !!}
                                            @if($errors->has('postal_code'))
                                                <span class="help-block">{{ $errors->first('postal_code') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>{{--row--}}

                                <div class="row">
                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                        <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                                            {!! Form::label('city', 'Cidade') !!}
                                            {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'Cidade']) !!}
                                            @if($errors->has('city'))
                                                <span class="help-block">{{ $errors->first('city') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                        <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                            {!! Form::label('phone', 'Telefone Fixo') !!}
                                            {!! Form::text('phone', null, ['class' => 'form-control', 'value' => "{{ old('dob')}}", 'data-mask' => '(99) 99999-9999'])!!}
                                            @if($errors->has('phone'))
                                                <span class="help-block">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group {{ $errors->has('area_code') ? 'has-error' : '' }}">
                                            {!! Form::label('area_code', 'Código de Área') !!}
                                            {!! Form::text('area_code', null, ['class' => 'form-control', 'placeholder' => 'Código de Área']) !!}
                                            @if($errors->has('area_code'))
                                                <span class="help-block">{{ $errors->first('area_code') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                        <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                                            {!! Form::label('country', 'País') !!}
                                            {!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'País']) !!}
                                            @if($errors->has('country'))
                                                <span class="help-block">{{ $errors->first('country') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                                            <label for="birth_date">Data Aniversário:</label>
                                            <input id="birth_date" type="date" class="form-control" name="birth_date" placeholder="Data de Aniversário:" value="{{ auth()->user()->birth_date }}" required>

                                            @if ($errors->has('birth_date'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('birth_date') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>{{--row--}}
                            </div>
                        </div>{{--card--}}
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="{{ asset('painel/img/full-screen-image-4.jpg') }}" />
                            </div>
                            <div class="content">
                                <div class="author">
                                    @if(Auth::user()->avatar)
                                        <img class="avatar border-gray" src="{{ Auth::user()->avatar }}" />
                                    @else
                                        <img class="avatar border-gray" src="{{ asset('painel/img/default-avatar.png') }}" />
                                    @endif
                                    <h4 class="title">{{ $user->name }}</h4>
                                    <hr>
                                    <p>{{ $user->email }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">

                            <div class="content">
                                <div class="author text-center">
                                    <h5>Ações</h5>
                                    <div class="profile-help">
                                        <a href="javascript:history.back()" class="btn btn-app btn-defaut btn-fill"><i class="fa fa-arrow-left"></i> Voltar</a>
                                        <button type="submit" class="btn btn-fill btn-info">Salvar <i class="fa fa-lg fa-save"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection