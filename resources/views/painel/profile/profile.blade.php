@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">

                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="card">
                            <div class="header">
                                <h3 class="text-left">{{ $user->name }}</h3>
                                <hr>
                            </div>
                            <div class="content">
                                {!! Form::open(['route' => 'painel.profile.update']) !!}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">Name</label>
                                    <input id="name" type="text" class="form-control" name="name" placeholder="Nome" value="{{ auth()->user()->name }}" disabled autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">E-Mail</label>
                                    <input id="email" type="email" class="form-control" name="email" placeholder="E-mail" value="{{ auth()->user()->email }}" disabled="disabled">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>{{--card--}}
                    </div>{{--col-8--}}

                    {{--sidebar--}}
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="{{ asset('painel/img/full-screen-image-4.jpg') }}" />
                            </div>
                            <div class="content">
                                <div class="author">
                                    <img class="avatar border-gray" src="{{ asset('painel/img/default-avatar.png') }}" />
                                    <p>{{ $user->email }}</p>
                                    <hr>

                                    <p class="small"><strong>Data de criação: </strong>{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y') }}</p>
                                    <p class="small"><strong>Alterado pela última vez em: </strong>{{ \Carbon\Carbon::parse($user->updated_at)->format('d/m/Y') }}</p>
                                </div>
                                <hr>
                                <br>

                                    <button type="submit" class="btn btn-primary btn-block">
                                        Atualizar Perfil
                                    </button>


                                {!! Form::close() !!}
                                <a href="{{ route('painel.profile.password') }}" class="btn btn-block btn-success btn-fill">Alterar Senha</a>
                            </div>
                        </div>
                    </div>
                </div>{{--.row--}}
            </div>
        </div>
    </div>

@endsection
