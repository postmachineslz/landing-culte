<div class="fresh-datatables">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('painel.models.pages.title')</th>
            <th>Status</th>
            <th>@lang('painel.models.pages.image')</th>
            <th class="text-right">@lang('painel.models.crud.actions')</th>
        </tr>
        </thead>

        <tbody>
            @foreach($pages as $page)
                <tr>
                    <td>{{ $page->id }}</td>
                    <td>{{ $page->title }}</td>
                    <td>{!! $page::getStatus($page->status, true) !!}</td>
                    <td>
                        @if($page->image)
                            <img src="{{ asset('/img/uploads/pages/' . $page->image) }}" width="100">
                        @else
                            <img src="{{ asset('/img/default-image.png') }}" width="80">
                        @endif
                    </td>

                    <td class="text-right">
                        <a href="{{ route('pages.show', ['id' => $page->id]) }}" class="btn btn-fill btn-info btn-sm view" data-toggle="tooltip" title="@lang('painel.models.crud.show')">
                            <i class="fa fa-eye"></i>
                        </a>

                        <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-warning btn-sm btn-fill" data-toggle="tooltip" title="@lang('painel.models.crud.edit')">
                            <i class="fa fa-pencil"></i>
                        </a>

                        {!! Form::open(['route' => ['pages.destroy', $page->id], 'method' => 'delete', 'class' => 'delete_form pull-right', 'id' => 'delete-btn']) !!}
                            <button type="submit" class="btn btn-fill btn-danger btn-sm delete-btn" data-toggle="tooltip" title="@lang('painel.models.crud.delete')">
                                <i class="fa fa-times"></i>
                            </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>