@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        {!! Form::model($page, [
                           'method'    => 'POST',
                           'route'     => 'pages.store',
                           'class'     => 'form-horizontal',
                           'enctype'   => 'multipart/form-data'
                        ]) !!}

                        @include('painel.pages._form')

                        {!! Form::close() !!}

                    </div>{{--.col-8--}}
                </div>{{--.row--}}
            </div>{{--container-fluid--}}
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection