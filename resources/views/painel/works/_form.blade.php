<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <div class="card">
            <div class="header">
                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                    <i class="fa fa-arrow-left fa-lg"></i> 
                    @lang('painel.menu.back')
                </a>
                <legend>{{ $work->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}</legend>
            </div>
            <div class="content">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('title', trans('painel.models.how_works.title')) !!}
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => trans('painel.models.how_works.title')]) !!}
                                @if($errors->has('title'))
                                    <span class="help-block">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group {{ $errors->has('icon') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('icon', trans('painel.models.how_works.image')) !!}
                                {!! Form::file('icon', null, ['class' => 'form-control', 'placeholder' => trans('painel.models.how_works.image')]) !!}
                                @if($errors->has('icon'))
                                    <span class="help-block">{{ $errors->first('icon') }}</span>
                                @endif
                                @if($work->icon)
                                    <br>
                                    <img src="{{ asset('/img/uploads/works/' . $work->icon) }}" width="200">
                                @endif
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                            <div class="col-lg-12">
                                {!! Form::label('description', trans('painel.models.how_works.description')) !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 4, 'placeholder' => trans('painel.models.how_works.description')]) !!}
                                @if($errors->has('description'))
                                    <span class="help-block">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- end content-->
        </div><!--  end card  -->

    </div>{{--.col-9--}}

    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="card">
            <div class="content">
                <div class="text-center">
                    <h5>@lang('painel.models.crud.actions')</h5>
                    <hr>
                    <button type="submit" class="btn btn-sm btn-fill btn-info"><i class="fa fa-save"></i>
                        {{ $work->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}
                    </button>
                    <a href="{{ route('works.index') }}" class="btn btn-sm btn-fill btn-warning">
                        <i class="fa fa-exclamation-triangle"></i> 
                        @lang('painel.menu.cancel')
                    </a>
                </div>
            </div>
        </div><!--  end card  -->
    </div>{{--.col-3--}}

</div>{{--.row--}}

