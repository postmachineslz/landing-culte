<div class="fresh-datatables">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('painel.models.how_works.title')</th>
                <th>@lang('painel.models.how_works.image')</th>
                <th class="text-right">@lang('painel.models.crud.actions')</th>
            </tr>
        </thead>
        <tbody>
            @foreach($works as $work)
                <tr>
                    <td>{{ $work->id }}</td>
                    <td>{{ $work->title }}</td>
                    <td>
                        @if($work->icon)
                            <img src="{{ asset('/img/uploads/works/' . $work->icon) }}" width="30">
                        @else
                            <img src="{{ asset('/img/default-image.png') }}" width="30">
                        @endif
                    </td>
                    <td class="text-right">
                        <a href="{{ route('works.show', $work->id) }}" class="btn btn-primary btn-sm btn-fill" title="@lang('painel.models.crud.show')" data-tooltip="tooltip">
                            <i class="fa fa-eye"></i>
                        </a>

                        @if($work->deleted_at == null)
                            <a href="{{ route('works.edit', $work->id) }}" class="btn btn-info btn-sm btn-fill" title="@lang('painel.models.crud.edit')" data-tooltip="tooltip">
                                <i class="fa fa-pencil"></i>
                            </a>
                        @endif

                        @if($work->deleted_at !== null)
                            {!! Form::open(['route' => ['works.restore', $work->id], 'method' => 'post', 'class' => 'pull-right', 'id' => 'restore-register']) !!}
                            <button type="submit" class="btn btn-fill btn-success btn-sm delete-btn" title="@lang('painel.models.crud.restore')" data-tooltip="tooltip">
                                <i class="fa fa-refresh"></i>
                            </button>
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route' => ['works.destroy', $work->id], 'method' => 'delete', 'class' => 'delete_form pull-right', 'id' => 'delete-btn']) !!}
                                <button type="submit" class="btn btn-fill btn-danger btn-sm delete-btn" title="@lang('painel.models.crud.delete')" data-tooltip="tooltip">
                                    <i class="fa fa-times"></i>
                                </button>
                            {!! Form::close() !!}
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>