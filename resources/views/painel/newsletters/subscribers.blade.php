@extends('layouts.app')

@section('title', $titlePage .' | '. config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                                    <i class="fa fa-arrow-left fa-lg"></i> Voltar
                                </a>
                                <legend>Newsletter</legend>
                            </div>

                            <div class="content">
                                @if($subscribers->count())
                                    <div class="fresh-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Email</th>
                                                <th>Data de cadastro</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($subscribers as $subscriber)
                                                <tr>
                                                    <td>#{{ $subscriber->id }}</td>
                                                    <td>{{ $subscriber->email }}</td>
                                                    <td>{{ $subscriber->created_at }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <hr>
                                        <div class="text-center">
                                            {{ $subscribers->appends( request()->query())->render() }}
                                        </div>
                                        <div class="text-right">
                                            <small>Total de <strong>{{ $subscribeCount }}</strong> {{ str_plural('Inscrito', $subscribeCount) }}</small>
                                        </div>
                                    </div>
                                @else
                                    <h4 class="text-center text-danger">
                                        Sem registros
                                    </h4>
                                @endif
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection
