<div class="row">
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="card">
            <div class="header">
                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                    <i class="fa fa-arrow-left fa-lg"></i>
                    @lang('painel.menu.back')
                </a>
                <legend>{{ $download->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}</legend>
            </div>

            <div class="content">

                {{-- IMAGE --}}
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                        {!! Form::label('file', 'PDF') !!}
                        {!! Form::file('file') !!}
                        @if($errors->has('file'))
                            <span class="help-block">{{ $errors->first('file') }}</span>
                        @endif
                    </div>
                </div>

                {{-- STATUS --}}
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        {!! Form::label('status', trans('painel.models.crud.status.active')) !!}
                        <br>
                        <div class="switch" data-on-label="" data-off-label="">
                            {{ Form::checkbox('status', true) }}
                        </div>
                        @if($errors->has('status'))
                            <span class="help-block">{{ $errors->first('status') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">

                </div>

            </div><!-- end content-->
        </div><!--  end card  -->

    </div>{{--.col-9--}}

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <div class="card">
            <div class="content">
                <div class="text-center">
                    <h5>@lang('painel.models.crud.actions')</h5>
                    <hr>
                    <button type="submit" class="btn btn-xs btn-fill btn-info"><i class="fa fa-save"></i>
                        {{ $download->exists ? trans('painel.models.crud.edit') : trans('painel.models.crud.edit') }}
                    </button>
                    <a href="{{ route('downloads.index') }}" class="btn btn-xs btn-fill btn-warning">
                        <i class="fa fa-exclamation-triangle"></i>
                        @lang('painel.menu.cancel')
                    </a>
                </div>
            </div>
        </div><!--  end card  -->
    </div>{{--.col-3--}}

</div>{{--.row--}}

