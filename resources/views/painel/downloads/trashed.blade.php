@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <div class="btn-group pull-right" role="group" aria-label="...">
                                    <a href="javascript:history.go(-1)" class="btn btn-default btn-sm btn-fill margin-left">
                                        <i class="fa fa-arrow-left fa-lg"></i>
                                        @lang('painel.menu.back')
                                    </a>

                                    <a href="{{ route('downloads.create') }}" class="btn btn-info btn-sm btn-fill margin-left">
                                        <i class="fa fa-plus fa-lg"></i> @lang('painel.models.crud.create')
                                    </a>
                                </div>
                                <legend>{{ $titlePage }}</legend>
                            </div>
                            <div class="content">
                                @include('painel.downloads.table')
                                <hr>
                                <div class="text-center">
                                    {{ $downloads->appends( request()->query())->render() }}
                                </div>
                                <div class="text-right">
                                    <small>{{ $downloads->count() }} {{ str_plural(trans('painel.menu.downloads'), $downloads->count()) }}</small>
                                </div>
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection
