<div class="fresh-datatables">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>PDF</th>
                <th>Status</th>
                <th class="text-right">@lang('painel.models.crud.actions')</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $download->id }}</td>
                <td>{{ $download->file }}</td>
                <td>{!! $download::getStatus($download->status, true) !!}</td>

                <td class="text-right">
                    <a href="{{ route('downloads.show', $download->id) }}" class="btn btn-primary btn-sm btn-fill" title="@lang('painel.models.crud.show')" data-tooltip="tooltip">
                        <i class="fa fa-eye"></i>
                    </a>

                    <a href="{{ route('downloads.edit', $download->id) }}" class="btn btn-info btn-sm btn-fill" title="@lang('painel.models.crud.edit')" data-tooltip="tooltip">
                        <i class="fa fa-pencil"></i>
                    </a>

                    <a href="{{ route('downloads.downloadFile', $download->id) }}" class="btn btn-warning btn-sm btn-fill" title="Download" data-tooltip="tooltip">
                        <i class="fa fa-download"></i>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>