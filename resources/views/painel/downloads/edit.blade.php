@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">

                        {!! Form::model($download, [
                            'method'    => 'PUT',
                            'route'     => ['downloads.update', $download->id],
                            'class'     => 'form-horizontal',
                            'enctype'   => 'multipart/form-data'
                        ]) !!}

                        @include('painel.downloads._form')

                        {!! Form::close() !!}

                    </div>{{--.col-8--}}
                </div>{{--.row--}}
            </div>{{--container-fluid--}}
        </div>{{--/content--}}
    </div>{{--main-panel--}}
@endsection
