@extends('layouts.app')

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                                    <i class="fa fa-arrow-left fa-lg"></i>
                                    @lang('painel.menu.back')
                                </a>
                                <legend>
                                    @lang('painel.menu.downloads')
                                </legend>
                            </div>
                            <div class="content">
                                @include('painel.downloads.table')
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->
            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}
@endsection

