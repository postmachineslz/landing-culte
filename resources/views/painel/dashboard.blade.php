@extends('layouts.app')

@section('title', $titlePage.' | '. config('app.name'))

@section('content')
    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    {{--ESTATISTICAS INSCRITOS--}}
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">
                                    @lang('painel.dashboard.subscribed-statistics.title')
                                </h4>
                                <p class="category">
                                    @lang('painel.dashboard.subscribed-statistics.subtitle')
                                </p>
                            </div>
                            <div class="content">
                                <div id="chartSubscribers" class="ct-chart "></div>
                            </div>
                            <div class="footer">
                                <div class="legend">
                                    <i class="fa fa-circle text-info"></i> @lang('painel.dashboard.subscribed-statistics.des1')
                                    <i class="fa fa-circle text-danger"></i> @lang('painel.dashboard.subscribed-statistics.des2')
                                </div>
                            </div>

                        </div>
                    </div>

                    {{--NOVOS INSCRITOS--}}
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">
                                    @lang('painel.dashboard.new-subscribers.title')
                                </h4>
                                <p class="category">
                                    @lang('painel.dashboard.new-subscribers.subtitle')
                                </p>
                            </div>
                            <div class="content">
                                <div id="chartHours" class="ct-chart"></div>
                            </div>
                            <div class="footer">
                                <div class="legend">
                                    <i class="fa fa-circle text-info"></i> @lang('painel.dashboard.new-subscribers.desc1')
                                    <i class="fa fa-circle text-danger"></i> @lang('painel.dashboard.new-subscribers.desc2')
                                </div>
                            </div>
                        </div>
                    </div>{{--col-8--}}

                    {{--DOWNLOADS--}}
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="card">
                            <div class="header">
                                @lang('painel.dashboard.downloads.title')
                                <p class="category">
                                    @lang('painel.dashboard.downloads.subtitle')
                                </p>
                            </div>
                            <div class="content">
                                <div id="chartDownloads" class="ct-chart "></div>
                            </div>
                            <div class="footer">
                                <h6>
                                    @lang('painel.dashboard.downloads.total')
                                </h6>
                                <span class="badge">
                                    {{ $totalDownloads->count() }}
                                </span>
                            </div>
                        </div>
                    </div>

                    {{--ACESSOS--}}
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="card">
                            <div class="header">
                                @lang('painel.dashboard.access.title')
                                <p class="category">
                                    @lang('painel.dashboard.access.subtitle')
                                </p>
                            </div>
                            <div class="content">
                                <div id="chartBehaviour" class="ct-chart "></div>
                            </div>
                            <div class="footer">
                                <h6>@lang('painel.dashboard.access.total')</h6>
                                <i class="fa fa-circle text-info"></i> @lang('painel.dashboard.access.desc1') <span class="badge">{{ $countAccessors }}</span>
                                <i class="fa fa-circle text-danger"></i> @lang('painel.dashboard.access.desc2') <span class="badge">{{ $countPageViews }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>{{--./content--}}
    </div>{{--./main-panel--}}

@endsection

@push('scripts')
    <script async defer type="text/javascript">
        app = {
            initCharts: function(){
                /*  **************** DOWNLOADS ******************** */
                var dataDownloads = {
                    labels: [
                        @foreach($downloads as $download)
                            {!! $download['mes'] !!} ,
                        @endforeach
                    ],
                    series: [
                        [
                            @foreach($downloads as $download)
                                {!! $download['total_download'] !!} ,
                            @endforeach
                        ]
                    ]
                };

                var optionsDownloads = {
                    lineSmooth: false,
                    axisY: {
                        offset: 40
                    },
                    low: 0,
                    high: 50
                };

                Chartist.Line('#chartDownloads', dataDownloads, optionsDownloads);

                /*  **************** ACESSOS ******************** */
                var dataViews = {
                    labels: [
                        @foreach($accessors as $accessor)
                            {!! date_format($accessor['date'], 'd') . ', ' !!}
                        @endforeach
                    ],
                    series: [
                        [
                            @foreach($accessors as $accessor)
                                {!! $accessor['visitors'] . ', ' !!}
                            @endforeach
                        ],
                        [
                            @foreach($accessors as $accessor)
                                {!! $accessor['pageViews'] . ', ' !!}
                            @endforeach
                        ]
                    ]
                };

                var optionsViews = {
                    lineSmooth: false,
                    axisY: {
                        offset: 30
                    },
                    low: 0,
                    high: 80
                };

                Chartist.Line('#chartBehaviour', dataViews, optionsViews);
            },

            initDashboardPageCharts: function(){
                /*  **************** ESTATISTICAS INSCRITOS ******************** */
                Chartist.Pie('#chartSubscribers', {
                    labels: [
                        '{!! $subscribers->count() ? $subscribers->count() : ''!!}', '{!! $canceledSubscribers->count() ? $canceledSubscribers->count() : ''!!}'
                    ],
                    series: [
                        {!! $subscribers->count() !!}, {!! $canceledSubscribers->count() !!}
                    ]
                });


                /*   **************** NOVOS INSCRITOS ********************    */
                var dataSales = {
                    labels: [
                        @foreach($newSubscribers as $newSubscriber)
                            "{!! date("F", mktime(0, 0, 0, $newSubscriber['week'], 10)) !!}" ,
                        @endforeach
                    ],
                    series: [
                        [
                            @php setlocale(LC_TIME, 'br'); @endphp
                            @foreach($newSubscribers as $newSubscriber)
                                {{ $newSubscriber['total_subscribers'] }} ,
                            @endforeach
                        ]
                    ]
                };

                var optionsSales = {
                    lineSmooth: true,
                    low: 0,
                    high: 50,
                    height: "245px",
                    axisX: {
                        showGrid: true,
                        offset: 40
                    },
                };

                var responsiveSales = [
                    ['screen and (max-width: 640px)', {
                        axisX: {
                            labelInterpolationFnc: function (value) {
                                return value[0];
                            }
                        }
                    }]
                ];

                Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);
            },
        }

        //
        $(document).ready(function(){
            app.initDashboardPageCharts();
            app.initCharts();
        });
    </script>
@endpush
