<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ trans('pages.meta.description') }}">
    <meta name="author" content="wicool.com.br">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

    <title>@yield('title', config('app.name'))</title>

    <meta property="og:url"           content="https://www.cultecoin.com" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="{{ config('app.name') }}" />
    <meta property="og:description"   content="{{ trans('pages.meta.description') }}" />
    <meta property="og:image"         content="https://www.cultecoin.com/img/logo-green.png" />

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}" rel="stylesheet">

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/assets/owl.carousel.min.css'>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.2.4/assets/owl.theme.default.css'>


    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>

</head>

<body id="page-top">

    @include('_partials.frontend.navigation')

    @yield('content')
    <script src='https://code.jquery.com/jquery-1.12.4.min.js'></script>
    {{--<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>--}}
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('vendor/scrollreveal/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/creative.min.js') }}"></script>
    <script src="{{ asset('vendor/carousel/multislider.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2//2.0.0-beta.2.4/owl.carousel.min.js'></script>


    @stack('scripts')

    <script type="text/javascript">
        {{----}}
        $('.owl-carousel').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            nav: true,
            navText: [
                "<img src=\"{{ asset('img/arrow-white-left.png') }}\">",
                "<img src=\"{{ asset('img/arrow-white-right.png') }}\">"
            ],
            autoplayHoverPause: true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                },
                600:{
                    items:1,
                },
                1000:{
                    items:3,
                    loop:true
                },
                1400:{
                    items:3,
                    loop:true
                }
            }
        });

        // carousel works
        $('#basicSlider').multislider({
            continuous: false,
            duration: 0,
            hoverPause: true,
            interval: 0,
        });

        $(document).ready(function () {
            var atual = 0;
            var limit = "{{ \App\Models\Work::get()->count() }}";

            $('#nextWork').click(function () {

                atual += 1;

                if (atual > limit) {
                    atual = 1;
                }

                console.log(atual);
                $('.item').removeClass('work-active');
                $('#iten-' + atual).addClass('work-active');

                $last = $('#iten-' + atual).hasClass("last");

                if ($last) {

                    $('#basicSlider').multislider('next');

                    $('#iten-' + atual).removeClass('last');

                    var prox = atual + 1;

                    if (prox > limit) {
                        prox = 1;
                        atual = 0;
                    }

                    $('#iten-' + prox).addClass('last');

                    // LIMITE COMEÇO
                    var inicio = atual - 4;

                    if (inicio <= 0) {
                        inicio = limit - (inicio * -1);
                    }

                    console.log('inicio: '+inicio);

                    $('.item').removeClass('first');
                    $('#iten-' + inicio).addClass('first');
                }
            });

            $('#prevWork').click(function () {
                atual -= 1;
                if (atual > limit || atual <= 0) {
                    atual = limit;
                }

                $('.item').removeClass('work-active');
                $('#iten-' + atual).addClass('work-active');

                $first = $('#iten-' + atual).hasClass("first");

                if ($first) {

                    console.log(atual);
                    $('#basicSlider').multislider('prev');
                    $('#iten-' + atual).removeClass('first');

                    var prox = atual - 1;

                    if (prox == limit || prox == 0) {
                        prox = limit;
                        atual = 1;
                    }

                    console.log('prox: ' + prox);

                    $('#iten-' + prox).addClass('first');

                    // LIMITE FIM

                    var fim = atual + 4;

                    if (fim > limit) {
                        fim = fim - limit;
                    }
                    console.log('fim '+fim);

                    $('.item').removeClass('last');
                    $('#iten-' + fim).addClass('last');
                }
            });
        });

        {{----}}
        $(document).ready(function() {
            $('#reason-carousel').carousel({
                pause: true,
                interval: 6000,
            });
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}");
        @endif
        @if(Session::has('danger'))
            toastr.error("{{ Session::get('danger') }}");
        @endif

        if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
            document.getElementById("scrollTop").style.display = "block";
        } else {
            document.getElementById("scrollTop").style.display = "none";
        }

        // countdown
        function makeTimer()
        {
            var endTime = new Date("August 10, 2018 12:00:00 PDT");
            var endTime = (Date.parse(endTime)) / 1000;

            var now = new Date();
            var now = (Date.parse(now) / 1000);

            var timeLeft = endTime - now;

            var days = Math.floor(timeLeft / 86400);
            var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
            var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
            var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

            if (hours < "10") { hours = "0" + hours; }
            if (minutes < "10") { minutes = "0" + minutes; }
            if (seconds < "10") { seconds = "0" + seconds; }

            $(".days").html(days);
            $(".hours").html(hours);
            $(".minutes").html(minutes);
            $(".seconds").html(seconds);

        }

        setInterval(function() { makeTimer(); }, 1000);

        // newsletter
        $('div.message').hide();
        $('#form-subscribers').submit(function(e) {
            e.preventDefault();
            var data = $(this).serialize();
            var url = "{{ route('newsletter.subscribe') }}";

            if (data.indexOf('=&') > -1) {
                $('div.message').hide().removeClass('success error').addClass('text-danger').html("@lang('messages.subscriber.error')").fadeIn('fast');
                return;
            }

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                beforeSend: startPreloader("Aguarde...")
            }).done(function(){
                $('div.message').hide().removeClass('success error').addClass('text-success').html("@lang('messages.subscriber.success')").fadeIn('fast');
                $('#form-subscribers').each (function(){
                    this.reset();
                });
            }).fail(function(){
                $('div.message').hide().removeClass('success error').addClass('text-danger').html("@lang('messages.subscriber.fail')").fadeIn('fast');
            }).always(function(){
                endPreloader();
            });
        });

        // download
        $('#form-download').submit(function(e) {
            e.preventDefault();
            var data = $(this).serialize();
            var url = "{{ route('downloads.downloadPDF') }}";

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                beforeSend: startPreloader("@lang('messages.download.wait')")
            }).done(function(){
                $("#successDownload").modal('show');
                $('#form-download').each (function(){
                    this.reset();
                });
            }).fail(function(){
                $('#message').hide().removeClass('success error').addClass('text-danger').html("@lang('messages.download.fail')").fadeIn('fast');
            }).always(function(){
                endPreloader();
            });
        });

        // contact
        $('#contact-form').submit(function(e) {
            e.preventDefault();
            var data = $(this).serialize();
            var url = "{{ route('contact.send') }}";

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                beforeSend: startPreloader("Enviando... " + "<i class=\"fa fa-spinner fa-2x\"></i>")
            }).done(function(){
                $('#message-contact').hide().removeClass('success error').addClass('text-success').html("@lang('messages.contact.success')").fadeIn('fast').fadeOut(10000);
                $('#contact-form').each (function(){
                    this.reset();
                });
            }).fail(function(){
                $('#message-contact').hide().removeClass('success error').addClass('text-danger').html("@lang('messages.contact.fail')").fadeIn('fast').fadeOut(10000);
            }).always(function(){
                endPreloader();
            });
        });

        function startPreloader(msgPreloader)
        {
            if(msgPreloader !== "") {
                $('.preloader').html(msgPreloader);
            }

            $('.preloader').show();
            $('.submit_btn').addClass('disabled');
        }

        function endPreloader()
        {
            $('.preloader').hide();
            $('.submit_btn').addClass('disabled');
        }

        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
    </script>

    <!-- Modal Success Dowload-->
    <div class="modal fade" id="successDownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">@lang('messages.download.title_modal')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        @lang('messages.download.success')
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="pull-right btn btn-secondary" data-dismiss="modal">@lang('messages.download.close_modal')</button>
                </div>
            </div>
        </div>
    </div>


    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119993788-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-119993788-1');
    </script>

</body>

</html>

