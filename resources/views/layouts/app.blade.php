<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="{{ asset('painel/img/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet"/>
    <title>@yield('title', config('app.name'))</title>
    <link href="{{ asset('painel/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('painel/css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>
    <link href="{{ asset('painel/css/demo.css') }}" rel="stylesheet"/>
    <link href="{{ asset('painel/css/pe-icon-7-stroke.css') }}" rel="stylesheet"/>
    <link href="{{ asset('painel/css/datepicker3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('painel/css/custom.css') }}" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/_all.css" rel="stylesheet">

    @stack('styles')

    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>
</head>
<body>

<div class="wrapper">
    @include('_partials.painel.sidebar')
    @yield('content')
</div>{{--wrapper--}}

<script src="{{ asset('painel/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('painel/js/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('painel/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('painel/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('painel/js/moment.min.js') }}"></script>
<script src="{{ asset('painel/js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('painel/js/bootstrap-selectpicker.js') }}"></script>
<script src="{{ asset('painel/js/bootstrap-checkbox-radio-switch-tags.js') }}"></script>
<script src="{{ asset('painel/js/chartist.min.js') }}"></script>
<script src="{{ asset('painel/js/bootstrap-notify.js') }}"></script>
<script src="{{ asset('painel/js/sweetalert2.js') }}"></script>
<script src="{{ asset('painel/js/jquery-jvectormap.js') }}"></script>
<script src="{{ asset('painel/js/jquery.bootstrap.wizard.min.js') }}"></script>
<script src="{{ asset('painel/js/bootstrap-table.js') }}"></script>
<script src="{{ asset('painel/js/jquery.datatables.js') }}"></script>
<script src="{{ asset('painel/js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('painel/js/light-bootstrap-dashboard.js') }}"></script>
<script src="{{ asset('painel/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('painel/js/Chart.min.js') }}"></script>
<script src="{{ asset('painel/js/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('painel/js/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('painel/js/input-mask/jquery.inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('painel/js/input-mask/jquery.inputmask.phone.extensions.js') }}"></script>
<script src="{{ asset('painel/js/input-mask/jquery.inputmask.regex.extensions.js') }}"></script>
<script src="{{ asset('painel/js/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.pt-BR.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

<script src="{{ asset('painel/js/demo.js') }}"></script>
<script src="{{ asset('painel/js/functions.js') }}"></script>

<script type="text/javascript">
    // SESSION MESSAGES
    @if(Session::has('success'))
        $.notify({
            icon: 'pe-7s-bell',
            message: "<b>@lang('painel.controllers.flash-messages.type.success')!</b> <br> {{ Session::get('success') }}"
        },{
            type: 'info',
            timer: 4000
        });
    @endif

    @if(Session::has('error-message'))
        $.notify({
            icon: 'pe-7s-attention',
            message: "<b>@lang('painel.controllers.flash-messages.type.danger')!</b> <br> {{ Session::get('error-message') }}"
        },{
            type: 'danger',
            timer: 4000
        });
    @endif

    @if(Session::has('info'))
        $.notify({
            icon: 'pe-7s-bell',
            message: "<b>@lang('painel.controllers.flash-messages.type.info')!</b> <br> {{ Session::get('info') }}"
        },{
            type: 'info',
            timer: 4000
        });
    @endif

    @if(Session::has('danger'))
        $.notify({
            icon: 'pe-7s-attention',
            message: "<b>@lang('painel.controllers.flash-messages.type.danger')!</b> <br> {{ Session::get('danger') }}"
        },{
            type: 'danger',
            timer: 4000
        });
    @endif

    @if(Session::has('warning'))
        $.notify({
            icon: 'pe-7s-attention',
            message: "<b>@lang('painel.controllers.flash-messages.type.warning')!</b> <br> {{ Session::get('warning') }}"
        },{
            type: 'warning',
            timer: 4000
        });
    @endif

    // delete
    $(document).on('click', '#delete-btn', function (e) {
        e.preventDefault();
        var self = $(this);
        swal({
            title: "@lang('painel.modal-confirm-restore.title')",
            text: "@lang('painel.modal-confirm-restore.desc')",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "@lang('painel.modal-confirm-delete.actions.not')",
            confirmButtonText: "@lang('painel.modal-confirm-delete.actions.yes')",
            closeOnConfirm: false
        }, function () {
            self.submit();
        });
    });

    // restore register
    $(document).on('click', '#restore-register', function (e) {
        e.preventDefault();
        var self = $(this);
        swal({
            title: "@lang('painel.modal-confirm-delete.title')",
            text: "@lang('painel.modal-confirm-delete.desc')",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "@lang('painel.modal-confirm-delete.actions.not')",
            confirmButtonText: "@lang('painel.modal-confirm-delete.actions.yes')",
            closeOnConfirm: false
        }, function () {
            self.submit();
        });
    });

    $(function () {

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    // datepicker
    !function(a){a.fn.datepicker.dates["pt-BR"]={
        days:["Domingo","Segunda","Terça","Quarta","Quinta","Sexta","Sábado"],
        daysShort:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
        daysMin:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
        months:["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
        monthsShort:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
        today:"Hoje",
        monthsTitle:"Meses",
        clear:"Limpar",
        format:"dd/mm/yyyy"}
    }(jQuery);

    $('.date_picker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        language: 'pt-BR',
        clearBtn: true,
        todayBtn: true,
        todayHighlight: true
    });

    // select2
    $(document).ready(function () {
        $('.select2').select2();
    });

    // tooltip
    $(function () {
        $('[data-tooltip="tooltip"]').tooltip()
    });

</script>

@stack('scripts')

</body>
</html>