<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('painel/img/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>@yield('title', config('app.name'))</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="{{ asset('painel/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('painel/css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>
    <link href="{{ asset('painel/css/demo.css') }}" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('painel/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />

</head>
<body>
    <div class="wrapper wrapper-full-page">
        @yield('content')
    </div>
</body>
</html>