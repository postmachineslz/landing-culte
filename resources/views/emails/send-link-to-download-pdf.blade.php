<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{{ config('app.name') }}</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%"
     style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
    <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
            <tbody>
            <tr>
                <td style="vertical-align: top; padding-bottom:0px;" align="center">
                    <a href="https://cultecoin.com" target="_blank">
                        <img class="img-responsive" src="https://cultecoin.com/img/logo-green.png" style="border:none; width: 200px;"/>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
            <tr>
                <td style="background:#fff; padding: 20px; color:#000; font-size: 14px; text-align:left;">
                    <p>Olá <strong>{{ $subscriber->name }}</strong></p>

                    <?php
                        $lang= $_SERVER['REQUEST_URI'];
                        $explode = explode('/', $lang, -1);
                        $ok = implode(",", $explode);
                    ?>
                    @if($ok == ',br')
                        <p>não</p>
                        <p>
                            Obrigado pelo seu interesse em nossa apresentação explicativa.
                            Neste material falamos sobre nossos objetivos e potencial de mercado, mostramos como vamos revolucionar o agronegócio brasileiro e explicamos detalhadamente a criptomoeda.
                            Você pode acessá-lo sempre que quiser através deste link e, se tiver dúvidas além do conteúdo do material, você pode esclarecê-las em nossa comunidade no Telegram.
                            Até logo!
                        </p>
                    @else
                        <p>
                            Thank you for your interest in our explanatory presentation.
                            In this material we talk about our objectives and market potential, we show how we are going to revolutionize the Brazilian agribusiness and we explain in detail the crypto.
                            You can access it whenever you want through this link and if you have questions beyond the content of the material, you can clarify them in our community on the Telegram.
                            See you later!
                        </p>
                    @endif

                    <div style="width: 100%; text-align: center">
                        <a href="{{ route('downloads.id-token', $token) }}" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #1AB394; border-radius: 40px; text-decoration:none;">
                            @if($ok == ',br') Baixar @else Download @endif
                        </a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        @if($ok == ',br')
            <div style="padding: 0 20px; background: #fff;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody>
                    <tr>
                        <td align="left" valign="top" colspan="2" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; padding-top: 20px; color:#666666;">
                            <span>Atenciosamente;</span><br>
                            <b>Equipe {{ config('app.name') }}</b><br/>
                            <small>Email de disparo automático . Favor não responder .</small>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
                <p> {{ config('app.name') }} - {{ date('Y') }} | Todos os Diretitos Reservados.<br>
            </div>
        @else
            <div style="padding: 0 20px; background: #fff;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody>
                    <tr>
                        <td align="left" valign="top" colspan="2" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; padding-top: 20px; color:#666666;">
                            <span>Tttentively;</span><br>
                            <b>Team {{ config('app.name') }}</b><br/>
                            <small>Email auto-trigger. Please do not respond.</small>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
                <p> {{ config('app.name') }} - {{ date('Y') }} | All rights reserved.<br>
            </div>
        @endif
    </div>
</div>
</body>
</html>