@extends('layouts.frontend')

@section('title', config('app.name').' | '. $titlePage)

@section('content')

    {{--HEADER--}}
    <div class="overlay"></div>
    <header class="masthead-page text-center text-white d-flex">
        <div class="container my-auto" style="z-index: 9999">
            <p class="text-left">Culte Coin > Faq</p>
            <a href="#faqList">
                <img src="{{ asset('img/mouse.gif') }}" alt="" class="text-center mouse-down-faq js-scroll-trigger">
            </a>
        </div>
    </header>
    <div id="faqList">
        <div class="container">
            <div id="accordion" role="tablist" class="container-faq">

                <h3 class="header-title">Faq</h3>

                @foreach($faqs as $faq)
                    <div class="card">
                        <div class="card-header" role="tab" id="heading-{{ $faq->id }}">
                            <h5 class="mb-0">
                                <a class="text-uppercase" data-toggle="collapse" href="#collapse-{{ $faq->id }}" aria-expanded="false" aria-controls="collapse-{{ $faq->id }}">
                                    {{ $faq->id }} - {{ $faq->question }}
                                </a>
                            </h5>
                        </div>
                        <div id="collapse-{{ $faq->id }}" class="collapse" role="tabpanel" aria-labelledby="heading-{{ $faq->id }}" data-parent="#accordion">
                            <div class="card-body">
                                {{ $faq->answer }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>{{--.container-faq--}}

            {{--DOWNLOAD--}}
            @include('_partials.frontend.download')
        </div>
    </div>

    @include('_partials.frontend.footer')

@endsection

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush
