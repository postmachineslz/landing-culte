<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-default btn-fill btn-sm">
                <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                <i class="fa fa-navicon visible-on-sidebar-mini"></i>
            </button>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('dashboard') }}">{{ config('app.name') }}</a>
        </div>
        <h3 class="title-page"> - {{ $titlePage }} </h3>


        <div class="pull-right" id="btnLogout">
            @foreach (config('app.locales') as $localeKey => $locale)
                @if ($localeKey !== app()->getLocale())
                    <a href="{{ route('locale.switch', $localeKey) }}" class="small text-muted">
                        @lang('painel.titles.change-lang') {{ $locale }}
                        <img src="{{ asset('img/' . $localeKey) . '.jpeg' }}" alt="{{ app()->getLocale() }}" style="margin-right: 25px;">
                    </a>
                @endif
            @endforeach

            <a href="{{ config('app.url')}}" class="btn btn-info btn-fill btn-xs hidden-xs" target="_blank">
                <i class="fa fa-home"></i> @lang('painel.titles.go-to-site')
            </a>

            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-fill btn-xs hidden-xs">
                <i class="fa fa-sign-out"></i> @lang('painel.titles.logout')
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</nav>