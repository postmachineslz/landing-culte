<div class="sidebar" data-color="default">
    <div class="logo">
        <a href="{{ route('dashboard') }}" class="logo-text">
            {{ config('app.name') }}
            <img src="{{ asset('img/' . app()->getLocale()) . '.jpeg' }}" alt="{{ app()->getLocale() }}" style="margin-left: 5px;">
        </a>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <a href="{{ route('painel.profile') }}">
                <div class="photo">
                    <img src="{{ asset('painel/img/default-avatar.png') }}" />
                </div>
                <div class="info">
                    <div class="text-center">
                        @if (auth()->check())
                            {{ auth()->user()->name }} <br>
                            <small>@lang('painel.titles.my-profile')</small>
                        @endif
                    </div>
                </div>
            </a>
        </div>

        <ul class="nav">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="pe-7s-graph"></i>
                    <p>Painel</p>
                </a>
            </li>

            {{--BANNERS--}}
            <li>
                <a data-toggle="collapse" href="#banenrs">
                    <i class="pe-7s-albums"></i>
                    <p>
                        @lang('painel.menu.banners')
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="banenrs">
                    <ul class="nav">
                        <li><a href="{{ route('banners.index') }}"><i class="fa fa-bars" aria-hidden="true"></i> @lang('painel.menu.list')</a></li>
                        <li><a href="{{ route('banners.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> @lang('painel.menu.create')</a></li>
                    </ul>
                </div>
            </li>

            {{--PAGES--}}
            <li>
                <a data-toggle="collapse" href="#pages">
                    <i class="pe-7s-folder"></i>
                    <p>
                        @lang('painel.menu.pages')
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="pages">
                    <ul class="nav">
                        <li><a href="{{ route('pages.index') }}"><i class="fa fa-bars" aria-hidden="true"></i> @lang('painel.menu.list')</a></li>
                        <li><a href="{{ route('pages.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> @lang('painel.menu.create')</a></li>
                    </ul>
                </div>
            </li>

            {{--WORKS--}}
            <li>
                <a data-toggle="collapse" href="#works">
                    <i class="pe-7s-config"></i>
                    <p>
                        @lang('painel.menu.how_works')
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="works">
                    <ul class="nav">
                        <li><a href="{{ route('works.index') }}"><i class="fa fa-bars" aria-hidden="true"></i> @lang('painel.menu.list')</a></li>
                        <li><a href="{{ route('works.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> @lang('painel.menu.create')</a></li>
                    </ul>
                </div>
            </li>

            {{--REASONS--}}
            <li>
                <a data-toggle="collapse" href="#reasons">
                    <i class="pe-7s-menu"></i>
                    <p>
                        @lang('painel.menu.reasons')
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="reasons">
                    <ul class="nav">
                        <li><a href="{{ route('reasons.index') }}"><i class="fa fa-bars" aria-hidden="true"></i> @lang('painel.menu.list')</a></li>
                        <li><a href="{{ route('reasons.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> @lang('painel.menu.create')</a></li>
                    </ul>
                </div>
            </li>

            {{--TEAMS--}}
            <li>
                <a data-toggle="collapse" href="#teams">
                    <i class="pe-7s-wallet"></i>
                    <p>
                        @lang('painel.menu.teams')
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="teams">
                    <ul class="nav">
                        <li><a href="{{ route('teams.index') }}"><i class="fa fa-bars" aria-hidden="true"></i> @lang('painel.menu.list')</a></li>
                        <li><a href="{{ route('teams.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> @lang('painel.menu.create')</a></li>
                    </ul>
                </div>
            </li>

            {{--WHITE PAPER--}}
            <li>
                <a href="{{ route('downloads.index') }}">
                    <i class="pe-7s-cloud-download"></i>
                    <p>@lang('painel.menu.downloads')</p>
                </a>
            </li>

            {{--FAQS--}}
            <li>
                <a data-toggle="collapse" href="#faq">
                    <i class="pe-7s-wallet"></i>
                    <p>
                        @lang('painel.menu.faqs')
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="faq">
                    <ul class="nav">
                        <li><a href="{{ route('faq.index') }}"><i class="fa fa-bars" aria-hidden="true"></i> @lang('painel.menu.list')</a></li>
                        <li><a href="{{ route('faq.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> @lang('painel.menu.create')</a></li>
                        <li><a href="{{ route('category-faq.index') }}"><i class="fa fa-plus" aria-hidden="true"></i> @lang('painel.menu.faq-category')</a></li>
                    </ul>
                </div>
            </li>

            {{-- NEWSLTTER--}}
            <li>
                <a href="{{ route('newsletters.subscribers') }}">
                    <i class="fa fa-envelope-o"></i>
                    <p>@lang('painel.menu.newsletters')</p>
                </a>
            </li>

            {{--USERS--}}
            <li>
                <a data-toggle="collapse" href="#users">
                    <i class="pe-7s-users"></i>
                    <p>
                        @lang('painel.menu.users')
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="users">
                    <ul class="nav">
                        <li><a href="{{ route('users.index') }}"><i class="fa fa-bars" aria-hidden="true"></i> @lang('painel.menu.list')</a></li>
                        <li><a href="{{ route('users.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> @lang('painel.menu.create')</a></li>
                    </ul>
                </div>
            </li>

            <hr class="user-profile-menu">

            <li class="hidden-lg hidden-md hidden-sm">
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('form-logout').submit();">
                    <i class="fa fa-sign-out"></i> @lang('painel.titles.logout')
                </a>
                <form id="form-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>{{--sidebar-wrapper--}}
</div>{{--sidebar--}}