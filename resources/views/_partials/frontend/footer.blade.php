<a href="#page-top" class="js-scroll-trigger">
    <div class="container">
        <img src="{{ asset('img/scroll-top.png') }}" alt="Ir para o topo" id="scrollTop">
    </div>
</a>

{{--FOOTER--}}
<footer id="footer">
    <div class="container">
        <div class="row">
            {{--ADDRESS--}}
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 d-none d-sm-block ">
                <h3>
                    @lang('pages.titles.address')
                </h3>
                <p>Pärnu mnt 148, Tallinn, Estoni, 11317 </p>
                <p>contato@culte.coin</p>
            </div>

            {{--LINKS--}}
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 d-none d-sm-block ">
                <h3>Links</h3>
                <ul>
                    <li>
                        <a href="{{ route('site') }}#about" class="js-scroll-trigger">
                            @lang('pages.menu.about')
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('site') }}#works" class="js-scroll-trigger">
                            @lang('pages.menu.works')
                        </a>
                    </li>
                    <li>
                        <a class="js-scroll-trigger" href="{{ route('site') }}#reasons">
                            @lang('pages.menu.reasons')
                        </a>
                    </li>
                    <li>
                        <a class="js-scroll-trigger" href="{{ route('site') }}#teams">
                            @lang('pages.menu.teams')
                        </a>
                    </li>
                    <li>
                        <a class="js-scroll-trigger" href="{{ route('site') }}#download">
                            @lang('pages.menu.download')
                        </a>
                    </li>
                    <li><a href="#" data-toggle="modal" data-target="#modalContact">
                            @lang('pages.menu.contact')
                        </a></li>
                </ul>
            </div>

            {{--FAQ--}}
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 d-none d-sm-block ">
                <div class="faq-menu-footer">

                <h3>Faq</h3>
                <ul>
                    <li>
                        <a href="{{ route('faq') }}">@lang('pages.menu.faq-all')</a>
                    </li>
                    @foreach($categoriesFaq as $category)
                        <li><a href="{{ route('faq.category', $category->slug) }}">{{ $category->name }}</a></li>
                    @endforeach
                </ul>
                </div>
            </div>

            {{--NEWS--}}
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <h3>
                    @lang('pages.titles.get-news')
                </h3>
                <p class="small">
                    @lang('pages.titles.text-news')
                </p>

                {!! Form::open(['url' => 'newsletter', 'method' => 'post', 'id' => 'form-subscribers', 'role' => 'form']) !!}
                    <div class="form-group">
                        {!! Form::text('name',null,['placeholder' => trans('pages.contact-form.labels.name'), 'class' => 'form-control', 'id' => 'newsletter_name', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::email('email',null,['placeholder' => trans('pages.contact-form.labels.email'), 'class' => 'form-control', 'id' => 'newsletter_email', 'required']) !!}
                    </div>

                    <button type="submit" class="submit_btn btn btn-primary pull-right" id="newsletter_submit">
                        {!! trans('pages.titles.btn-news') !!}
                    </button>

                    <div class="message"><h4><strong></strong></h4></div>
                    <div class="preloader" style="display: none;">Preloader...</div>
                {!! Form::close() !!}
            </div>
        </div>{{--row--}}

        {{--social links--}}
        <div class="social-footer-links">
            <a href="http://fb.me/cultecoin" target="_blank"><i class="fa fa-facebook-f fa-2x"></i></a>
            <a href="https://www.instagram.com/cultecoin/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
            <a href="https://www.linkedin.com/company/cultecoin/" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
        </div>
    </div>

    <div class="copyright text-center d-none d-sm-block ">
        <p class="title-copyright">
            @lang('pages.titles.text-copyright')
        </p>
        <a href="{{ route('site') }}"><img src="{{ asset('img/logo-green.png') }}" class="logo-footer" width="80"></a>
    </div>

    <!-- Modal Contact-->
    <div class="modal fade bd-example-modal-lg" id="modalContact" tabindex="-1" role="dialog" aria-labelledby="modalContactTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalContactTitle">
                        <img src="{{ asset('img/logo-gray-green.png') }}">
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('img/close-modal.png') }}" alt="" width="40">
                    </button>
                </div>

                {!! Form::open(['url' => 'contact', 'method' => 'post', 'id' => 'contact-form', 'role' => 'form']) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label for="name">@lang('pages.contact-form.labels.name')</label>
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('pages.contact-form.labels.name')]) !!}
                                    @if($errors->has('name'))
                                        <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                    <label for="phone">@lang('pages.contact-form.labels.phone')</label>
                                    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => trans('pages.contact-form.labels.phone'), 'data-mask' => trans('pages.contact-form.labels.phone')]) !!}
                                    @if($errors->has('phone'))
                                        <span class="help-block text-danger">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label for="email">@lang('pages.contact-form.labels.email')</label>
                                    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('pages.contact-form.labels.email')]) !!}
                                    @if($errors->has('email'))
                                        <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>{{--.row--}}

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('function') ? 'has-error' : '' }}">
                                    <label for="function">@lang('pages.contact-form.labels.function')</label>
                                    {!! Form::text('function', null, ['class' => 'form-control', 'placeholder' => trans('pages.contact-form.labels.function')]) !!}
                                    @if($errors->has('function'))
                                        <span class="help-block text-danger">{{ $errors->first('function') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                                    <label for="subject">@lang('pages.contact-form.labels.subject')</label>
                                    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => trans('pages.contact-form.labels.subject')]) !!}
                                    @if($errors->has('subject'))
                                        <span class="help-block text-danger">{{ $errors->first('subject') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>{{--.row--}}

                        <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                            <label for="message">@lang('pages.contact-form.labels.message')</label>
                            {!! Form::textarea('message', null, ['rows' => 4, 'class' => 'form-control', 'placeholder' => trans('pages.contact-form.labels.message')]) !!}
                            @if($errors->has('message'))
                                <span class="help-block text-danger">{{ $errors->first('message') }}</span>
                            @endif
                        </div>

                        <div id="message-contact"><h4><strong></strong></h4></div>
                        <div class="preloader" style="display: none;">Preloader...</div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary text-left submit_btn">
                            @lang('pages.contact-form.labels.btn-text') <img src="{{ asset('img/arrow-modal.png') }}">
                        </button>

                        <a href="https://fb.me/cultecoin"><i class="fa fa-facebook-f"></i></a>
                        <a href="https://instagram.com/cultecoin"><i class="fa fa-instagram"></i></a>
                        <a href="https://instagram.com/cultecoin"><i class="fa fa-linkedin"></i></a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</footer>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'Z3GvodLrRc';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->


