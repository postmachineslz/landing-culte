<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container" style="z-index: 9999">
        <a class="navbar-brand js-scroll-trigger" href="{{ route('site') }}#page-top">
            <img src="{{ asset('img/logo-white.png') }}" alt="{{ config('app.name') }}" class="img-logo-white">
            <img src="{{ asset('img/logo-green.png') }}" alt="{{ config('app.name') }}" class="img-logo-green">
        </a>

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-navicon"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" target="_blank" href="https://t.me/joinchat/Gq1SXRDZXkOaIr8Kc9xlVg">Telegram</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('site') }}#works">
                        @lang('pages.menu.works')
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('site') }}#reasons">
                        @lang('pages.menu.reasons')
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('site') }}#posts">
                        @lang('pages.menu.posts')
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('site') }}#teams">
                        @lang('pages.menu.teams')
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('site') }}#download">
                        @lang('pages.menu.download')
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="languagesDropdown" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                        <img src="{{ asset('img/' . app()->getLocale()) . '.jpeg' }}" alt="{{ app()->getLocale() }}" style="margin-right: 5px;">
                        {{--{{ app()->getLocale() }} <span class="caret"></span>--}}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="languagesDropdown">
                        @foreach (config('app.locales') as $localeKey => $locale)
                            @if ($localeKey != app()->getLocale())
                                <a class=" dropdown-item" href="{{ route('locale.switch', $localeKey) }}">
                                    <img src="{{ asset('img/' . $localeKey) . '.jpeg' }}" alt="{{ app()->getLocale() }}" style="margin-right: 5px;">
                                    {{ strtoupper($localeKey) }}
                                </a>
                            @endif
                        @endforeach
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>