<section id="download">
    <div class="container">
        <div class="box-download">
            <h2 class="section-heading">
                @lang('pages.titles.download')
            </h2>
        </div>

        <div class="content-section form-download" id="form-mobile">
            <div class="content-section caixa">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img src="{{ asset('img/download.jpg') }}" class="img-responsive">
                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <h3>@lang('pages.titles.learn-more-about')</h3>
                        <p>@lang('pages.titles.text-download')</p>

                        {!!  Form::open(['id' => 'form-download', 'role' => 'form']) !!}

                        <input type="hidden" value="{{ $download->id }}" name="id">

                        <div class="form-group">
                            {!! Form::text('name', null,['placeholder' => trans('pages.contact-form.labels.name'), 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::email('email', null,['placeholder' => trans('pages.contact-form.labels.email'), 'class' => 'form-control', 'required']) !!}
                        </div>

                        <button type="submit" class="btn btn-default submit_btn">
                            {!! trans('pages.titles.btn-download') !!}
                        </button>

                        <div id="message" style="padding: 10px; margin-top: 20px; text-align: center"></div>
                        <div class="preloader" style="display: none;">Preloader...</div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>{{--caixa--}}
        </div>{{--.form-download--}}

    </div>
</section>