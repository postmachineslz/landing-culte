@extends('layouts.login')

@section('title', config('app.name') . ' | ' . 'Página não encontrada')

@section('content')

    <div class="full-page register-page" data-color="default" data-image="{{ asset('assets/img/full-screen-image-4.jpg') }}">
        <div class="content">
            <div class="container">
                <div class="header-text">
                    <h1>OPS!!!</h1>
                    <h3>Página não encontrada</h3>
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">

                        <div class="text-center">
                            @if (session('status'))
                                <div class="alert alert-info">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <p style="color: #fff;">Esta página não pode ser encontrada.</p>

                            <br>
                            <a href="javascript:history.go(-1)" class="btn btn-neutral btn-round btn-fil btn-wd">
                                <i class="fa fa-arrow-left"></i>Voltar
                            </a>

                        </div>{{-- .user-profile --}}
                    </div>
                </div>
            </div>
        </div>
    </div>{{--full-page login-page--}}

@endsection

