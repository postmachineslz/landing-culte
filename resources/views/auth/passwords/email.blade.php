@extends('layouts.login')

@section('title', 'Resetar Senha | ' . config('app.name'))

@section('content')

    <div class="full-page register-page" data-color="default" data-image="{{ asset('assets/img/full-screen-image-4.jpg') }}">
        <div class="content">
            <div class="container">
                <div class="header-text">
                    {{-- <img src="{{ asset('assets/img/logo-white.png') }}" width="250"> --}}
                    <h1>{{ strtoupper(config('app.name')) }}</h1>
                    <p>Resetar Senha</p>
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form role="form" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="text-center">
                                @if (session('status'))
                                    <div class="alert alert-info">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <h4 style="color: #fff;">Digite seu e-mail</h4>

                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <br>
                                <button type="submit" class="btn btn-neutral btn-round btn-fil btn-wd btn-block">Enviar link de redefinição de senha</button>
                                <br>
                                <a href="{{ route('login') }}" class="btn btn-neutral btn-round btn-fil btn-wd">Voltar para login</a>

                            </div>{{-- .user-profile --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>{{--full-page login-page--}}

@endsection
