@extends('layouts.login')

@section('title', 'Resetar Senha | ' . config('app.name'))

@section('content')
    <div class="full-page login-page" data-color="default">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="text-white" for="email">E-Mail</label>

                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="text-white" for="password">Senha</label>

                                
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                                
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="text-white" for="password-confirm">Confirmação de Senha</label>
                                
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif
                                
                            </div>

                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-neutral btn-round btn-fil btn-wd">
                                    Salvar
                                </button>
                            </div>
                        </form>
                    </div>{{-- .col-4 --}}
                </div>{{-- .row --}}
            </div>{{-- ./container --}}
        </div>{{-- ./content --}}

    </div>{{--full-page login-page--}}
@endsection
