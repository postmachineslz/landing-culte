@extends('layouts.login')

@section('title', 'Login | ' . config('app.name'))

@section('content')

    <div class="full-page login-page" data-color="default">
        <div class="content" style="padding-top: 100px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">

                        @if($message = Session::get('success'))
                            <div class="alert alert-success">
                                {{$message }}
                            </div>
                        @endif
                        @if($message = Session::get('warning'))
                            <div class="alert alert-warning">
                                {{$message }}
                            </div>
                        @endif

                        <form role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="card">
                                <div class="text-center">
                                    <img src="{{ asset('img/logo-gray-green.png') }}" height="110"/>
                                </div>
                                <div class="content">
                                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">

                                        <label for="email">Login</label>

                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Login">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password">Senha</label>

                                        <input id="password" type="password" class="form-control" name="password" required placeholder="Senha">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            Relembre-me
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <p><a href="{{ route('password.request') }}">Esqueceu sua senha?</a></p>
                                    </div>
                                </div>

                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-fill btn-default btn-block btn-wd">Login</button>
                                </div>
                            </div>
                        </form>
                    </div>{{--col-6--}}
                </div>
            </div>
        </div>
    </div>{{--full-page login-page--}}
@endsection