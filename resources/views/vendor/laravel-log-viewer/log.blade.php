@extends('layouts.app')

@php $titlePage = 'Logs do Sistema'; @endphp

@section('title', $titlePage . ' | ' . config('app.name'))

@section('content')

    <div class="main-panel">

        @include('_partials.painel.top-navbar')

        <div class="content">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <a href="javascript:history.go(-1)" class="pull-right btn btn-default btn-xs btn-fill">
                                    <i class="fa fa-arrow-left fa-lg"></i> Voltar
                                </a>
                                <legend>Logs do Sistema</legend>
                            </div>
                            <div class="content table-container">

                                @if ($logs === null)
                                    <h4 class="text-warning text-center">Arquivo de log> 50M, faça o download.</h4>
                                @else
                                    <table id="table-log" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Data</th>
                                                <th>Content</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($logs as $key => $log)
                                                <tr data-display="stack{{{$key}}}">
                                                    <td class="text-{{{$log['level_class']}}}">
                                                        <span class="fa fa-{{{$log['level_img']}}}" aria-hidden="true"></span> &nbsp;{{$log['level']}}
                                                    </td>
                                                    <td class="date">
                                                        {{ \Carbon\Carbon::parse($log['date'])->format('d/m/Y H:i') }}
                                                    </td>
                                                    <td class="text">
                                                        @if ($log['stack'])
                                                            {{--<button type="button" class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2" data-display="stack{{{$key}}}">--}}
                                                                {{--<span class="fa fa-search"></span>--}}
                                                            {{--</button>--}}
                                                        @endif
                                                        {{{$log['text']}}}
                                                        @if (isset($log['in_file'])) <br/>{{{$log['in_file']}}}@endif
                                                        @if ($log['stack'])
                                                        <div class="stack" id="stack{{{$key}}}"
                                                             style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
                                                        </div>@endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                                    <div class="p-3">
                                        @if($current_file)
                                            <a href="?dl={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}">
                                                <span class="fa fa-download"></span>Baixar
                                            </a>
                                            -
                                            <a id="delete-log" href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}">
                                                <span class="fa fa-trash"></span> Remover laravel.log
                                            </a>
                                            @if(count($files) > 1)
                                                -
                                                <a id="delete-all-log" href="?delall=true"><span class="fa fa-trash"></span> Remover todos os arquivos</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->
            </div>
        </div>{{--/content--}}
    </div>{{--main-panel--}}

@endsection

@push('scripts')
    {{--<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>--}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.table-container tr').on('click', function () {
                $('#' + $(this).data('display')).toggle();
            });
            $('#table-log').DataTable({
                "order": [1, 'desc'],
                "stateSave": true,
                "stateSaveCallback": function (settings, data) {
                    window.localStorage.setItem("datatable", JSON.stringify(data));
                },
                "stateLoadCallback": function (settings) {
                    var data = JSON.parse(window.localStorage.getItem("datatable"));
                    if (data) data.start = 0;
                    return data;
                },
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json",
                    "lengthMenu": "_MENU_ Registros por página",
                    "zeroRecords": "Nenhum resgistro encontrado - Desculpe",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "nenhum registro",
                    "searchPlaceholder": "Pesquisar",
                }
            });
            $('#delete-log, #delete-all-log').click(function () {
                return confirm('Tem certeza disso?');
            });
        });
    </script>
@endpush