<?php

Auth::routes();

Route::match(['get', 'post'], 'register', function () {
    return redirect()->back();
});


// SWITCH LANGUAGE
Route::name('locale.switch')->get('lang/{locale}', 'LocaleController@switch');

// HOME
Route::get('/', 'HomeController@index')->name('site');

// FAQ
Route::get('/faq', 'FaqsController@index')->name('faq');
Route::get('/faq/{slug}', 'FaqsController@category')->name('faq.category');

// NEWSLETTER
Route::post('newsletter', 'SubscribersController@subscribe')->name('newsletter.subscribe');

// WHITE PAPER DOWNLOAD
Route::post('download', 'DownloadsController@download')->name('downloads.downloadPDF');
Route::get('downloads/{token}', 'DownloadsController@downloadPDF')->name('downloads.id-token');

// CONTACT
Route::post('contact', 'ContactMailController@sendMail')->name('contact.send');


// PAINEL ROUTES
Route::group(['middleware' => ['auth'], 'prefix' => 'painel', 'namespace' => 'Painel'], function () {
    // LOGS
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    // DASHBOARD
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    // NEWSLETTER
    Route::get('newsletter/subscribers', 'SubscribersController@subscribers')->name('newsletters.subscribers');

    // BANNERS
    Route::resource('banners', 'BannersController');

    // FAQS
    Route::get('faq/trashed', 'FaqsController@trashed')->name('faq.trashed');
    Route::post('faq/{id}/restore', 'FaqsController@restore')->name('faq.restore');
    Route::post('faq/{id}/forceDelete', 'FaqsController@forceDelete')->name('faq.forceDelete');
    Route::resource('faq', 'FaqsController');

    // CATEGORY FAQS
    Route::resource('category-faq', 'CategoryFaqController');

    // PAGES
    Route::get('pages/trashed', 'PagesController@trashed')->name('pages.trashed');
    Route::post('pages/{id}/restore', 'PagesController@restore')->name('pages.restore');
    Route::post('pages/{id}/forceDelete', 'PagesController@forceDelete')->name('pages.forceDelete');
    Route::resource('pages', 'PagesController');

    // TEAMS
    Route::get('teams/trashed', 'TeamsController@trashed')->name('teams.trashed');
    Route::post('teams/{id}/restore', 'TeamsController@restore')->name('teams.restore');
    Route::post('teams/{id}/forceDelete', 'TeamsController@forceDelete')->name('teams.forceDelete');
    Route::resource('teams', 'TeamsController');

    // REASONS
    Route::get('reasons/trashed', 'ReasonsController@trashed')->name('reasons.trashed');
    Route::post('reasons/{id}/restore', 'ReasonsController@restore')->name('reasons.restore');
    Route::post('reasons/{id}/forceDelete', 'ReasonsController@forceDelete')->name('reasons.forceDelete');
    Route::resource('reasons', 'ReasonsController');

    // WORKS
    Route::get('works/trashed', 'WorksController@trashed')->name('works.trashed');
    Route::post('works/{id}/restore', 'WorksController@restore')->name('works.restore');
    Route::post('works/{id}/forceDelete', 'WorksController@forceDelete')->name('works.forceDelete');
    Route::resource('works', 'WorksController');

    // DOWLOADS
    Route::get('downloads/{id}/downloadFile', 'DownloadsController@downloadFile')->name('downloads.downloadFile');
    Route::get('downloads/trashed', 'DownloadsController@trashed')->name('downloads.trashed');
    Route::post('downloads/{id}/restore', 'DownloadsController@restore')->name('downloads.restore');
    Route::post('downloads/{id}/forceDelete', 'DownloadsController@forceDelete')->name('downloads.forceDelete');
    Route::resource('downloads', 'DownloadsController');

    // USERS
    Route::get('users/trashed', 'UserController@trashed')->name('users.trashed');
    Route::post('users/{id}/restore', 'UserController@restore')->name('users.restore');
    Route::post('users/{id}/forceDelete', 'UserController@forceDelete')->name('users.forceDelete');
    Route::resource('users', 'UserController');

    // PROFILE
    Route::get('meu-perfil', 'ProfileController@profile')->name('painel.profile');
    Route::post('atualizar-perfil', 'ProfileController@profileUpdate')->name('painel.profile.update');
    Route::get('alterar-senha', 'ProfileController@password')->name('painel.profile.password');
    Route::post('atualizar-senha', 'ProfileController@passwordUpdate')->name('painel.profile.password.update');
});

