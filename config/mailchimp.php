<?php

return [
    /*
    |--------------------------------------------------------------------------
    | API Secret Key
    |--------------------------------------------------------------------------
    |
    | The api secret key to access Mailchimp. If you don't know the API key, find it here:
    | "http://kb.mailchimp.com/accounts/management/about-api-keys#Find-or-Generate-Your-API-Key"
    |
     */

    'apikey' => env('MAILCHIMP_API_KEY', '3d6c6ec6450481a15317cc107280ae5a-us18')
];
